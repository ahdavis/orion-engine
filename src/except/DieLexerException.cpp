/*
 * DieLexerException.cpp
 * Implements an exception that is thrown when a die equation lexer
 * encounters an error
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieLexerException.h"

//other include
#include <sstream>

//namespace usage statement
using namespace Orion;

//constructor
DieLexerException::DieLexerException(int pos, char badChar)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << pos << ':';
	ss << " Unknown character '" << badChar;
	ss << "' in die equation";
	this->errMsg = ss.str();
}

//destructor
DieLexerException::~DieLexerException() {
	//no code needed
}

//copy constructor
DieLexerException::DieLexerException(const DieLexerException& dle)
	: errMsg(dle.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
DieLexerException& DieLexerException::operator=(
					const DieLexerException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* DieLexerException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
