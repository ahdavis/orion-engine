/*
 * DieParserException.cpp
 * Implements an exception that is thrown when a die equation parser
 * encounters an error
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieParserException.h"

//other include
#include <sstream>

//namespace usage statement
using namespace Orion;

//constructor
DieParserException::DieParserException(int pos,
					const std::string& expected,
					const std::string& found)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << pos << ':';
	ss << " Die equation syntax error (expected ";
	ss << expected << ',';
	ss << " found ";
	ss << found << ')';
	this->errMsg = ss.str();
}

//destructor
DieParserException::~DieParserException() {
	//no code needed
}

//copy constructor
DieParserException::DieParserException(const DieParserException& dpe)
	: errMsg(dpe.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
DieParserException& DieParserException::operator=(
					const DieParserException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* DieParserException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
