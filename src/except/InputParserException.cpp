/*
 * InputParserException.cpp
 * Implements an exception that is thrown when a parser finds an error
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputParserException.h"

//other include
#include <sstream>

//namespace usage statement
using namespace Orion;

//constructor
InputParserException::InputParserException(const InputToken& first,
					const InputToken& badFollow)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << badFollow.getValue();
	ss << " (a ";
	ss << badFollow.getType();
	ss << ")";
	ss << " does not follow ";
	ss << first.getValue();
	ss << " (a ";
	ss <<  first.getType();
	ss << ").";
	this->errMsg = ss.str();
}

//destructor
InputParserException::~InputParserException() {
	//no code needed
}

//copy constructor
InputParserException::InputParserException(
				const InputParserException& ipe)
	: errMsg(ipe.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
InputParserException& InputParserException::operator=(
				const InputParserException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* InputParserException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
