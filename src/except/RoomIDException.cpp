/*
 * RoomIDException.cpp
 * Implements an exception that is thrown when 
 * no more unique room IDs are available
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "RoomIDException.h"

//namespace usage statement
using namespace Orion;

//overridden what method - called when the exception is thrown
const char* RoomIDException::what() const throw() {
	return "Out of unique map room IDs!";
}

//end of implementation
