/*
 * InputParserException.h
 * Declares an exception that is thrown when a parser finds an error
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include "../cmd/InputToken.h"

//namespace declaration
namespace Orion {
	//class declaration
	class InputParserException final : public std::exception {
		//public fields and methods
		public:
			//constructor
			InputParserException(const InputToken& first,
					const InputToken& badFollow);

			//destructor
			~InputParserException();

			//copy constructor
			InputParserException(
					const InputParserException& ipe);

			//assignment operator
			InputParserException& operator=(
					const InputParserException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
