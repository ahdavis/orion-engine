/*
 * RoomIDException.h
 * Declares an exception that is thrown when 
 * no more unique room IDs are available
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <exception>

//namespace declaration
namespace Orion {
	//class declaration
	class RoomIDException final : public std::exception {
		//public fields and methods
		public:
			//called when the exception is thrown
			const char* what() const throw() override;

		//no private fields or methods
	};
}

//end of header
