/*
 * InputLexerException.cpp
 * Implements an exception that is thrown when an input lexer errors
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputLexerException.h"

//other include
#include <sstream>

//namespace usage statement
using namespace Orion;

//constructor
InputLexerException::InputLexerException(const std::string& badText)
	: errMsg() //zero the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "The command \"" 
	   << badText
	   << "\" could not be understood.";
	this->errMsg = ss.str();
}

//destructor
InputLexerException::~InputLexerException() {
	//no code needed
}

//copy constructor
InputLexerException::InputLexerException(const InputLexerException& ile)
	: errMsg(ile.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
InputLexerException& InputLexerException::operator=(
					const InputLexerException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* InputLexerException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
