/*
 * DieIntNode.cpp
 * Implements a class that represents an integer die equation AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieIntNode.h"

//other include

//namespace usage statement
using namespace Orion;

//constructor
DieIntNode::DieIntNode(int newValue)
	: DieASTNode(nullptr, nullptr), value(newValue) //init the field
{
	//no code needed
}

//destructor
DieIntNode::~DieIntNode() {
	//no code needed
}

//copy constructor
DieIntNode::DieIntNode(const DieIntNode& din)
	: DieASTNode(din), value(din.value) //copy the field
{
	//no code needed
}

//assignment operator
DieIntNode& DieIntNode::operator=(const DieIntNode& src) {
	DieASTNode::operator=(src); //call the superclass assign operator
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//overridden evaluate operator - evaluates the node
int DieIntNode::evaluate() const {
	return this->value; //return the value field
}

//end of implementation
