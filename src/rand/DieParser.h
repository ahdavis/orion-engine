/*
 * DieParser.h
 * Declares a class that parses die equations
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include <string>
#include "DieLexer.h"
#include "DieToken.h"
#include "DieASTNode.h"

//namespace declaration
namespace Orion {
	//class declaration
	class DieParser {
		//public fields and methods
		public:
			//constructor
			explicit DieParser(const DieLexer& newLexer);

			//destructor
			virtual ~DieParser();

			//copy constructor
			DieParser(const DieParser& dp);

			//assignment operator
			DieParser& operator=(const DieParser& src);

			//parses the input and returns an AST
			//that represents it
			std::shared_ptr<DieASTNode> parse();

		//protected fields and methods
		protected:
			//methods
			
			//processes a token and gets the next token
			void eat(const std::string& tokenType);

			//parses a factor
			std::shared_ptr<DieASTNode> factor();

			//parses a term
			std::shared_ptr<DieASTNode> term();

			//parses an equation
			std::shared_ptr<DieASTNode> eqn();

			//fields
			
			//the lexer that processes the input
			DieLexer lexer;
			
			//the current token being parsed
			DieToken curToken;
	};
}

//end of header
