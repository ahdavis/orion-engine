/*
 * DieLexer.h
 * Declares a class that represents a die equation lexer
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include "DieToken.h"

//namespace declaration
namespace Orion {
	//class declaration
	class DieLexer {
		//public fields and methods
		public:
			//constructor
			explicit DieLexer(const std::string& eqn);

			//destructor
			virtual ~DieLexer();

			//copy constructor
			DieLexer(const DieLexer& dl);

			//assignment operator
			DieLexer& operator=(const DieLexer& src);

			//getter method
			
			//gets the current position in the text
			int getPos() const;

			//other method

			//returns the next token consumed from the input
			DieToken nextToken();

		//protected fields and methods
		protected:
			//methods
			
			//advances the lexer to the next character
			void advance(); 

			//skips whitespace in the input
			void skipWhitespace();

			//returns an integer consumed from the input
			std::string integer();

			//fields
			std::string text; //the text being lexed
			int pos; //the current position in the input
			char curChar; //the current character being lexed
	};
}

//end of header
