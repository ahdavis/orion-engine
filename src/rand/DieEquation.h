/*
 * DieEquation.h
 * Declares a class that represents a die equation
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace Orion {
	//class declaration
	class DieEquation {
		//public fields and methods
		public:
			//constructor
			DieEquation(const std::string& newEqn);

			//destructor
			virtual ~DieEquation();

			//copy constructor
			DieEquation(const DieEquation& de);

			//assignment operator
			DieEquation& operator=(const DieEquation& src);

			//getter method
			
			//gets the equation string
			const std::string& getEqn() const;

			//setter method
			
			//sets the equation string
			void setEqn(const std::string& newEqn);

			//other method
			
			//evaluates the equation
			int evaluate() const;

		//protected fields and methods
		protected:
			//field
			std::string eqn; //the equation string
	};
}

//end of header
