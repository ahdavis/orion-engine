/*
 * DieEquation.cpp
 * Implements a class that represents a die equation
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieEquation.h"

//other includes
#include "DieASTNode.h"
#include "DieLexer.h"
#include "DieParser.h"

//namespace usage statement
using namespace Orion;

//constructor
DieEquation::DieEquation(const std::string& newEqn)
	: eqn() //zero the field
{
	//and set the equation string
	this->setEqn(newEqn);
}

//destructor
DieEquation::~DieEquation() {
	//no code needed
}

//copy constructor
DieEquation::DieEquation(const DieEquation& de)
	: eqn(de.eqn) //copy the field
{	
	//no code needed
}

//assignment operator
DieEquation& DieEquation::operator=(const DieEquation& src) {
	this->eqn = src.eqn; //assign the equation field
	return *this; //and return the instance
}

//getEqn method - gets the equation string
const std::string& DieEquation::getEqn() const {
	return this->eqn; //return the equation field
}

//setEqn method - sets the equation string
void DieEquation::setEqn(const std::string& newEqn) {
	this->eqn = newEqn; //update the equation string
}

//evaluate method - evaluates the equation
int DieEquation::evaluate() const {
	//create a lexer for the equation
	DieLexer lexer(this->eqn);

	//create a parser from the lexer
	DieParser parser(lexer);

	//parse the equation
	auto ast = parser.parse();

	//and return its evaluation
	return ast->evaluate();
}

//end of implementation
