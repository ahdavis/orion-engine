/*
 * DieASTNode.h
 * Declares an abstract class that represents a die equation AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <memory>

//namespace declaration
namespace Orion {
	//class declaration
	class DieASTNode {
		//public fields and methods
		public:
			//constructor
			DieASTNode(
			const std::shared_ptr<DieASTNode>& newLeft,
			const std::shared_ptr<DieASTNode>& newRight);

			//destructor
			virtual ~DieASTNode();

			//copy constructor
			DieASTNode(const DieASTNode& dan);

			//assignment operator
			DieASTNode& operator=(const DieASTNode& src);

			//evaluates the node
			virtual int evaluate() const = 0;

		//protected fields and methods
		protected:
			//fields
			
			//the left branch of the AST
			std::shared_ptr<DieASTNode> left; 

			//the right branch of the AST
			std::shared_ptr<DieASTNode> right;
	};
}

//end of header
