/*
 * DieToken.h
 * Declares a class that represents a die equation token
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace Orion {
	//class declaration
	class DieToken {
		//public fields and methods
		public:
			//static field
			static const DieToken EOL; //an EOL token

			//constructor
			DieToken(const std::string& newType,
				const std::string& newValue);

			//destructor
			virtual ~DieToken();

			//copy constructor
			DieToken(const DieToken& dt);

			//assignment operator
			DieToken& operator=(const DieToken& src);

			//getter methods
			
			//gets the type of the token
			const std::string& getType() const;

			//gets the value of the token
			const std::string& getValue() const;

		//protected fields and methods
		protected:
			//fields
			std::string type; //the type of the token
			std::string value; //the value of the token
	};
}

//end of header
