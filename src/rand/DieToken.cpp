/*
 * DieToken.cpp
 * Implements a class that represents a die equation token
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieToken.h"

//namespace usage statement
using namespace Orion;

//static field initialization
const DieToken DieToken::EOL = DieToken("EOL", "");

//constructor
DieToken::DieToken(const std::string& newType,
			const std::string& newValue)
	: type(newType), value(newValue) //init the fields
{
	//no code needed
}

//destructor
DieToken::~DieToken() {
	//no code needed
}

//copy constructor
DieToken::DieToken(const DieToken& dt)
	: type(dt.type), value(dt.value) //copy the fields
{
	//no code needed
}

//assignment operator
DieToken& DieToken::operator=(const DieToken& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//getType method - gets the type of the token
const std::string& DieToken::getType() const {
	return this->type; //return the type field
}

//getValue method - gets the value of the token
const std::string& DieToken::getValue() const {
	return this->value; //return the value field
}	

//end of implementation
