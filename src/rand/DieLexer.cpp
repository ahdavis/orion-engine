/*
 * DieLexer.cpp
 * Implements a class that represents a die equation lexer
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieLexer.h"

//other includes
#include <cctype>
#include "../except/DieLexerException.h"

//namespace usage statement
using namespace Orion;

//constructor
DieLexer::DieLexer(const std::string& eqn)
	: text(eqn), pos(0), curChar('\0') //init the fields
{
	//and init the current character
	this->curChar = this->text[this->pos];
}

//destructor
DieLexer::~DieLexer() {
	//no code needed
}

//copy constructor
DieLexer::DieLexer(const DieLexer& dl)
	: text(dl.text), pos(dl.pos), curChar(dl.curChar) //copy the fields
{
	//no code needed
}

//assignment operator
DieLexer& DieLexer::operator=(const DieLexer& src) {
	this->text = src.text; //assign the text field
	this->pos = src.pos; //assign the position field
	this->curChar = src.curChar; //assign the current character
	return *this; //and return the instance
}

//getPos method - gets the current position in the text
int DieLexer::getPos() const {
	return this->pos; //return the position field
}

//nextToken method - returns the next token consumed from the input
DieToken DieLexer::nextToken() {
	//loop through the text and generate tokens
	while(this->curChar != '\0') {
		//check for whitespace
		if(std::isspace(this->curChar)) {
			this->skipWhitespace();
			continue;
		}

		//check for an addition operator
		if(this->curChar == '+') {
			this->advance();
			return DieToken("PLUS", "+");
		}

		//check for a subtraction operator
		if(this->curChar == '-') {
			this->advance();
			return DieToken("MINUS", "-");
		}

		//check for a die operator
		if((this->curChar == 'd') || 
			(this->curChar == 'D')) {
			this->advance();
			return DieToken("DIE", "d");
		}

		//check for a left parenthesis
		if(this->curChar == '(') {
			this->advance();
			return DieToken("LPAREN", "(");
		}

		//check for a right parenthesis
		if(this->curChar == ')') {
			this->advance();
			return DieToken("RPAREN", ")");
		}

		//check for an integer
		if(std::isdigit(this->curChar)) {
			return DieToken("INT", this->integer());
		}

		//if control reaches here, then
		//an unknown character was found
		//so we throw an error
		throw DieLexerException(this->pos, this->curChar);
	}

	//when control reaches here, 
	//the end of the input has been reached
	//so return an EOL token
	return DieToken::EOL;
}

//protected advance method - advances the lexer to the next character
void DieLexer::advance() {
	this->pos++; //advance the position index

	//and handle end of input
	if(this->pos > (this->text.length() - 1)) {
		this->curChar = '\0';
	} else {
		this->curChar = this->text[this->pos];
	}
}

//protected skipWhitespace method - skips whitespace in the input
void DieLexer::skipWhitespace() {
	while(std::isspace(this->curChar) && (this->curChar != '\0')) {
		this->advance();
	}
}

//protected integer method - returns an integer consumed from the input
std::string DieLexer::integer() {
	//declare the integer string
	std::string ret; 

	//assemble the integer string
	while(std::isdigit(this->curChar) && (this->curChar != '\0')) {
		ret += this->curChar; //append the current character
		this->advance(); //and advance the lexer
	}

	//and return the integer string
	return ret;
}

//end of implementation
