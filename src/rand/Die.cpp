/*
 * Die.cpp
 * Implements a class that represents an any-sided die
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Die.h"

//other includes
#include <cstdlib>
#include <ctime>

//namespace usage statement
using namespace Orion;

//default constructor
Die::Die()
	: Die(20) //call the other constructor
{
	//no code needed
}

//second constructor
Die::Die(int newSides)
	: sides(), value() //default the fields
{
	//seed the RNG
	std::srand(std::time(NULL));

	//set the side count
	this->setSides(newSides);

	//and roll the die
	this->roll();
}

//destructor
Die::~Die() {
	//no code needed
}

//copy constructor
Die::Die(const Die& d)
	: sides(d.sides), value(d.value) //copy the fields
{
	//no code needed
}

//assignment operator
Die& Die::operator=(const Die& src) {
	this->sides = src.sides; //assign the side count
	this->value = src.value; //assign the value
	return *this; //and return the instance
}

//getSides method - gets the side count of the die
int Die::getSides() const {
	return this->sides; //return the sides field
}

//getValue method - gets the value of the die
int Die::getValue() const {
	return this->value; //return the value field
}

//setSides method - sets the side count of the die
void Die::setSides(int newSides) {
	//validate the side count
	if(newSides < 1) {
		newSides = 20;
	}

	//and update the side count
	this->sides = newSides;
}

//roll method - rolls the die and returns the rolled value
int Die::roll() {
	//roll the die
	this->value = (std::rand() % this->sides) + 1;

	//and return the new value
	return this->value;
}

//end of implementation
