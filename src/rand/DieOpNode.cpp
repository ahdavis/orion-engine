/*
 * DieOpNode.cpp
 * Implements a class that represents a die equation operator AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieOpNode.h"

//other include
#include "Die.h"

//namespace usage statement
using namespace Orion;

//constructor
DieOpNode::DieOpNode(const std::shared_ptr<DieASTNode>& left,
			const std::shared_ptr<DieASTNode>& right,
			DieOpType newType)
	: DieASTNode(left, right), type(newType) //init the field
{
	//no code needed
}

//destructor
DieOpNode::~DieOpNode() {
	//no code needed
}

//copy constructor
DieOpNode::DieOpNode(const DieOpNode& don)
	: DieASTNode(don), type(don.type) //copy the field
{
	//no code needed
}

//assignment operator
DieOpNode& DieOpNode::operator=(const DieOpNode& src) {
	DieASTNode::operator=(src); //call the superclass assign operator
	this->type = src.type; //assign the type field
	return *this; //and return the instance
}

//overridden evaluate method - evaluates the node
int DieOpNode::evaluate() const {
	//declare the return value
	int ret = 0;

	//handle different operations
	switch(this->type) {
		//addition operator
		case DieOpType::ADD: {
			//get the operands
			int lhs = this->left->evaluate();
			int rhs = this->right->evaluate();

			//and calculate the result
			ret = lhs + rhs;

			break;
		}

		//subtraction operator
		case DieOpType::SUB: {
			//get the operands
			int lhs = this->left->evaluate();
			int rhs = this->right->evaluate();

			//and calculate the result
			ret = lhs - rhs;

			break;
		}

		//die operator
		case DieOpType::DIE: {
			//get the roll count
			int rolls = this->left->evaluate();

			//get the number of sides on the die
			int sides = this->right->evaluate();

			//create the proper die
			Die die(sides);

			//and roll the die
			for(int i = 0; i < rolls; i++) {
				ret += die.getValue();
				die.roll();
			}

			break;
		}

		//unknown operation type (should not be reached)
		default: {
			break;
		}
	}

	//and return the populated return value
	return ret;
}

//end of implementation
