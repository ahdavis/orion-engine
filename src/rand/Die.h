/*
 * Die.h
 * Declares a class that represents an any-sided die
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace Orion {
	//class declaration
	class Die {
		//public fields and methods
		public:
			//default constructor
			//constructs with a
			//side count of 20
			Die();

			//constructs with a given side count
			//if newSides is less than 1,
			//then the side count will be 20.
			explicit Die(int newSides);

			//destructor
			virtual ~Die();

			//copy constructor
			Die(const Die& d);

			//assignment operator
			Die& operator=(const Die& src);

			//getter methods
			
			//gets the number of sides on the die
			int getSides() const;

			//gets the current value of the die
			int getValue() const;

			//setter method
			
			//sets the number of sides on the die
			//if newSides is less than 1, then
			//the new side count will be 20.
			void setSides(int newSides);

			//other method
			
			//rolls the die and returns the value rolled
			int roll();

		//protected fields and methods
		protected:
			//fields
			int sides; //the number of sides on the die
			int value; //the current value of the die
	};
}

//end of header
