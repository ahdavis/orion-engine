/*
 * DieASTNode.cpp
 * Implements an abstract class that represents a die equation AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieASTNode.h"

//namespace usage statement
using namespace Orion;

//constructor
DieASTNode::DieASTNode(const std::shared_ptr<DieASTNode>& newLeft,
			const std::shared_ptr<DieASTNode>& newRight)
	: left(newLeft), right(newRight) //init the fields
{
	//no code needed
}

//destructor
DieASTNode::~DieASTNode() {
	//no code needed
}

//copy constructor
DieASTNode::DieASTNode(const DieASTNode& dan)
	: left(dan.left), right(dan.right) //copy the fields
{
	//no code needed
}

//assignment operator
DieASTNode& DieASTNode::operator=(const DieASTNode& src) {
	this->left = src.left; //assign the left branch
	this->right = src.right; //assign the right branch
	return *this; //and return the instance
}

//evaluate method is abstract

//end of implementation
