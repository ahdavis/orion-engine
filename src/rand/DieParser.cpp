/*
 * DieParser.cpp
 * Implements a class that parses die equations
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "DieParser.h"

//other includes
#include "../except/DieParserException.h"
#include "DieIntNode.h"
#include "DieOpNode.h"
#include "DieOpType.h"

//namespace usage statement
using namespace Orion;

//constructor
DieParser::DieParser(const DieLexer& newLexer)
	: lexer(newLexer), curToken("", "") //init the fields
{
	//and get the first token
	this->curToken = this->lexer.nextToken();
}

//destructor
DieParser::~DieParser() {
	//no code needed
}

//copy constructor
DieParser::DieParser(const DieParser& dp)
	: lexer(dp.lexer), curToken(dp.curToken) //copy the fields
{
	//no code needed
}

//assignment operator
DieParser& DieParser::operator=(const DieParser& src) {
	this->lexer = src.lexer; //assign the lexer
	this->curToken = src.curToken; //assign the current token
	return *this; //and return the instance
}

//parse method - parses the input and returns its AST
std::shared_ptr<DieASTNode> DieParser::parse() {
	return this->eqn(); //call the eqn method
}

//protected eat method - processes the current token and gets the next one
void DieParser::eat(const std::string& tokenType) {
	//verify the current token
	if(this->curToken.getType() == tokenType) {
		//and get the next token
		this->curToken = this->lexer.nextToken();
	} else { //if the type doesn't match
		//then throw an exception
		throw DieParserException(this->lexer.getPos(),
						tokenType,
					this->curToken.getType());
						
	}
}

//protected factor method - parses a factor
std::shared_ptr<DieASTNode> DieParser::factor() {
	//save the current token
	DieToken token = this->curToken;

	//process factor tokens
	if(token.getType() == "INT") { //integer token
		this->eat("INT");
		return std::make_shared<DieIntNode>(
				std::stoi(token.getValue()));
	} else if(token.getType() == "LPAREN") { //left parenthesis
		this->eat("LPAREN");
		auto node = this->eqn();
		this->eat("RPAREN");
		return node;
	} else { //unknown factor token
		throw DieParserException(this->lexer.getPos(),
						"INT or LPAREN",
						token.getType());
	}
}

//protected term method - parses a term
std::shared_ptr<DieASTNode> DieParser::term() {
	//parse a factor
	auto node = this->factor();

	//parse a die operator
	if(this->curToken.getType() == "DIE") {
		this->eat("DIE");
		node = std::make_shared<DieOpNode>(node, this->factor(),
						DieOpType::DIE);
	}

	//and return the node
	return node;
}

//protected eqn method - parses an equation
std::shared_ptr<DieASTNode> DieParser::eqn() {
	//parse a term
	auto node = this->term();

	//parse addition and subtraction operators
	while((this->curToken.getType() == "PLUS") ||
		(this->curToken.getType() == "MINUS")) {
		//save the current token
		DieToken token = this->curToken;

		//get the type of operation
		DieOpType type = DieOpType::ADD;

		//process different operators
		if(token.getType() == "PLUS") {
			this->eat("PLUS");
			type = DieOpType::ADD;
		} else if(token.getType() == "MINUS") {
			this->eat("MINUS");
			type = DieOpType::SUB;
		}

		//and generate the AST
		node = std::make_shared<DieOpNode>(node, this->term(),
							type);
	}

	//and return the node
	return node;
}

//end of implementation
