/*
 * DieOpNode.h
 * Declares a class that represents a die equation operator AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "DieASTNode.h"
#include "DieOpType.h"

//namespace declaration
namespace Orion {
	//class declaration
	class DieOpNode : public DieASTNode {
		//public fields and methods
		public:
			//constructor
			DieOpNode(const std::shared_ptr<DieASTNode>& left,
				const std::shared_ptr<DieASTNode>& right,
				DieOpType newType);

			//destructor
			virtual ~DieOpNode();

			//copy constructor
			DieOpNode(const DieOpNode& don);

			//assignment operator
			DieOpNode& operator=(const DieOpNode& src);

			//evaluates the node
			int evaluate() const override;

		//protected fields and methods
		protected:
			//field 
			DieOpType type; //the type of operation this is
	};
}

//end of header
