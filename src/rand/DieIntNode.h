/*
 * DieIntNode.h
 * Declares a class that represents an integer die equation AST node
 * Created on 8/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include "DieASTNode.h"

//namespace declaration
namespace Orion {
	//class declaration
	class DieIntNode : public DieASTNode {
		//public fields and methods
		public:
			//constructor
			explicit DieIntNode(int newValue);

			//destructor
			virtual ~DieIntNode();

			//copy constructor
			DieIntNode(const DieIntNode& din);

			//assignment operator
			DieIntNode& operator=(const DieIntNode& src);

			//evaluates the node
			int evaluate() const override;

		//protected fields and methods
		protected:
			//field
			int value; //the value of the node
	};
}

//end of header
