/*
 * CustomEvent.h
 * Declares a class that represents a custom event
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include "Event.h"

//namespace declaration
namespace Orion {
	//class declaration
	class CustomEvent : public Event {
		//public fields and methods
		public:
			//constructor
			explicit CustomEvent(int newID);

			//destructor
			virtual ~CustomEvent();

			//copy constructor
			CustomEvent(const CustomEvent& ce);

			//assignment operator
			CustomEvent& operator=(const CustomEvent& src);

			//getter method
			
			//gets the event ID
			int getID() const;

		//protected fields and methods
		protected:
			//field
			int id; //the event ID
	};
}

//end of header
