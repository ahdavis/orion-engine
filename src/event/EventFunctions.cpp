/*
 * EventFunctions.cpp
 * Implements functions that interact with the event queue
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the header
#include "EventFunctions.h"

//other include
#include "EventManager.h"

//PollEvent function - polls the next event from the queue
bool Orion::PollEvent(Orion::Event& e) {
	//poll the event manager
	return Orion::EventManager::getInstance().pollEvent(e);
}

//PushEvent function - pushes a custom event onto the queue
void Orion::PushEvent(const Orion::CustomEvent& ce) {
	//push the event onto the queue
	Orion::EventManager::getInstance().pushEvent(ce);
}

//HasPendingEvents function - returns whether events exist on the queue
bool Orion::HasPendingEvents() {
	//call EventManager::isEmpty
	return !Orion::EventManager::getInstance().isEmpty();
}

//ClearPendingEvents function - clears the event queue
void Orion::ClearPendingEvents() {
	//call EventManager::clear
	Orion::EventManager::getInstance().clear();
}

//end of implementation
