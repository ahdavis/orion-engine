/*
 * Event.h
 * Declares a class that represents a game event
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include "EventType.h"

//namespace declaration
namespace Orion {
	//class declaration
	class Event {
		//public fields and methods
		public:
			//constructs with the type NullEvent
			Event();

			//constructs with an event type
			explicit Event(EventType newType);

			//destructor
			virtual ~Event();

			//copy constructor
			Event(const Event& e);

			//assignment operator
			Event& operator=(const Event& src);

			//getter method
			
			//gets the type of the event
			EventType getType() const;

		//protected fields and methods
		protected:
			//field
			EventType type; //the type of the event
	};
}

//end of header
