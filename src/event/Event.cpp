/*
 * Event.cpp
 * Implements a class that represents a game event
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Event.h"

//namespace usage statement
using namespace Orion;

//default constructor
Event::Event()
	: Event(EventType::NullEvent) //call the other constructor
{
	//no code needed
}

//second constructor
Event::Event(EventType newType)
	: type(newType) //init the field
{
	//no code needed
}

//destructor
Event::~Event() {
	//no code needed
}

//copy constructor
Event::Event(const Event& e)
	: type(e.type) //copy the field
{
	//no code needed
}

//assignment operator
Event& Event::operator=(const Event& src) {
	this->type = src.type; //assign the type field
	return *this; //and return the instance
}

//getType method - gets the type of the event
EventType Event::getType() const {
	return this->type; //return the type field
}

//end of implementation
