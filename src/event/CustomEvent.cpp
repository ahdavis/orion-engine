/*
 * CustomEvent.cpp
 * Implements a class that represents a custom event
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "CustomEvent.h"

//other include
#include "EventType.h"

//namespace usage statement
using namespace Orion;

//constructor
CustomEvent::CustomEvent(int newID)
	: Event(EventType::CustomEvent), id(newID) //init the fields
{
	//no code needed
}

//destructor
CustomEvent::~CustomEvent() {
	//no code needed
}

//copy constructor
CustomEvent::CustomEvent(const CustomEvent& ce)
	: Event(ce), id(ce.id) //copy the fields
{
	//no code needed
}

//assignment operator
CustomEvent& CustomEvent::operator=(const CustomEvent& src) {
	Event::operator=(src); //call the superclass assignment operator
	this->id = src.id; //assign the event ID
	return *this; //and return the instance
}

//getID method - gets the ID of the event
int CustomEvent::getID() const {
	return this->id; //return the ID field
}

//end of implementation
