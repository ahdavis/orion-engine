/*
 * EventManager.cpp
 * Implements a class that manages game events
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "EventManager.h"

//namespace usage statement
using namespace Orion;

//private constructor
EventManager::EventManager()
	: events() //init the field
{
	//no code needed
}

//destructor
EventManager::~EventManager() {
	this->clear(); //clear the queue
}

//static getInstance method - gets an instance of the class
EventManager& EventManager::getInstance() {
	static EventManager instance; //will exist until process is closed
	return instance;
}

//pollEvent method - polls the next event on the queue
bool EventManager::pollEvent(Event& e) {
	//handle an empty queue
	if(this->isEmpty()) {
		return false;
	}

	//get the event on top of the queue
	e = this->events.front();

	//remove that event from the queue
	this->events.pop();

	//and return whether events remain in the queue
	return !this->isEmpty();
}

//pushEvent method - pushes an event onto the queue
void EventManager::pushEvent(const Event& e) {
	this->events.push(e); //push the event onto the queue
}

//isEmpty method - returns whether events remain on the queue
bool EventManager::isEmpty() const {
	return this->events.empty(); //return whether the queue is empty
}

//clear method - clears the event queue
void EventManager::clear() {
	//loop and clear the event queue
	while(!this->isEmpty()) {
		this->events.pop();
	}
}

//end of implementation
