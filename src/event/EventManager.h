/*
 * EventManager.h
 * Declares a class that manages game events
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "Event.h"
#include <queue>

//namespace declaration
namespace Orion {
	//class declaration
	class EventManager final {
		//public fields and methods
		public:
			//constructor is private
			
			//destructor
			~EventManager();

			//delete the copy constructor
			EventManager(const EventManager& em) = delete;

			//delete the assignment operator
			EventManager& operator=(
					const EventManager& src) = delete;

			//gets the instance of the class
			static EventManager& getInstance();

			//polls the next event on the event queue
			//and puts it into the argument
			//returns whether events remain on the queue
			//after the event is polled
			bool pollEvent(Event& e);

			//pushes an event onto the queue
			void pushEvent(const Event& e);

			//returns whether the event queue is empty
			bool isEmpty() const;

			//clears the event queue
			void clear();

		//private fields and methods
		private:
			//constructor
			EventManager();

			//field
			std::queue<Event> events; //the event queue
	};
}

//end of header
