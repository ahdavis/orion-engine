/*
 * EventFunctions.h
 * Declares functions that interact with the event queue
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "Event.h"
#include "CustomEvent.h"

//namespace declaration
namespace Orion {
	//function declarations
	
	//polls the next event from the queue
	//and puts it in the argument
	//returns whether the queue is empty
	//after polling the event
	bool PollEvent(Event& e);

	//pushes a custom event onto the queue
	//only works with event objects that
	//inherit from CustomEvent
	void PushEvent(const CustomEvent& ce);

	//returns whether the event queue is empty
	bool HasPendingEvents();

	//clears the event queue
	void ClearPendingEvents();
}

//end of header
