/*
 * EventType.h
 * Defines an enum that represents a game event type
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace Orion {
	//enum definition
	enum class EventType {
		NullEvent,   //denotes an empty event
		RoomEnter,   //emitted when an entity enters a room
		RoomLeave,   //emitted when an entity leaves a room
		EntityDie,   //emitted when an entity dies
		MapLoad,     //emitted when a new map is loaded
		CustomEvent, //a custom event created by the engine's user
		UserInput,   //emitted when user input is detected
		ItemPickup,  //emitted when an entity picks up an item
		ItemDrop     //emitted when an entity drops an item
	};
}

//end of header
