/*
 * Direction.cpp
 * Implements a function that gets the name of a direction
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "Direction.h"

namespace Orion {
	//nameForDirection function - gets the name of a Direction
	std::string nameForDirection(Direction d) {
		//declare the return value
		std::string ret = "";
	
		//populate it
		switch(d) {
			case Direction::East: {
				ret = "East";
				break;
			}
			case Direction::North: {
				ret = "North";
				break;
			}
			case Direction::Northeast: {
				ret = "Northeast";
				break;
			}
			case Direction::Northwest: {
				ret = "Northwest";
				break;
			}
			case Direction::South: {
				ret = "South";
				break;
			}
			case Direction::Southeast: {
				ret = "Southeast";
				break;
			}
			case Direction::Southwest: {
				ret = "Southwest";
				break;
			}
			case Direction::West: {
				ret = "West";
				break;
			}
			default: {
				break;
			}
		}
	
		//and return the populated return value
		return ret;
	}
}

//end of implementation
