/*
 * Door.h
 * Declares a class that represents a door between map rooms
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "Direction.h"
#include <iostream>

//namespace declaration
namespace Orion {
	//forward declare the Room class
	class Room;

	//class declaration
	class Door {
		//public fields and methods
		public:
			//constructor
			Door(const Room& entry,
				const Room& exit,
				Direction exitDirection);

			//destructor
			virtual ~Door();

			//copy constructor
			Door(const Door& d);

			//assignment operator
			Door& operator=(const Door& src);

			//getter methods
			
			//gets the ID of the entry room
			int getEntryID() const;

			//gets the ID of the exit room
			int getExitID() const;

			//gets the entry direction of the door
			Direction getEntryDirection() const;

			//gets the exit direction of the door
			Direction getExitDirection() const;

			//other methods
			
			//output operator
			friend std::ostream& operator<<(std::ostream& os,
							const Door& d);

		//protected fields and methods
		protected:
			//method
			
			//returns the opposite Direction
			//to the direction field
			Direction flipDirection() const;

			//fields
			
			//the ID of the entry room
			int entryID;

			//the ID of the exit room
			int exitID; 

			//the direction of the door relative
			//to the entry room
			Direction direction; 
	};
}

//end of header
