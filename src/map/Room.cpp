/*
 * Room.cpp
 * Implements a class that represents a map room
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Room.h"

//other includes
#include <climits>
#include "../except/RoomIDException.h"

//namespace usage statement
using namespace Orion;

//static field initialization
int Room::curID = 0;

//first constructor - constructs with no connected rooms
Room::Room(const std::string& newName,
		const std::string& newDescription)
	: Room(newName, newDescription, std::vector<Door>())
{
	//no code needed
}

//second constructor - constructs with connected rooms
Room::Room(const std::string& newName,
		const std::string& newDescription,
		const std::vector<Door>& newDoors)
	: id(-1), name(newName), description(newDescription),
		doors(newDoors)
{
	//populate the room ID field
	if(curID == INT_MAX) { //no more unique room IDs
		throw RoomIDException();
	} else {
		this->id = curID++;
	}
}

//destructor
Room::~Room() {
	//no code needed
}

//copy constructor
Room::Room(const Room& r)
	: id(r.id), name(r.name), description(r.description), 
		doors(r.doors)
{
	//no code needed
}

//assignment operator
Room& Room::operator=(const Room& src) {
	this->id = src.id; //assign the id field
	this->name = src.name; //assign the name field
	this->description = src.description; //assign the description field
	this->doors = src.doors; //assign the doors field
	return *this; //and return the instance
}

//getName method - gets the name of the room
const std::string& Room::getName() const {
	return this->name; //return the name field
}

//getDescription method - gets the description of the room
const std::string& Room::getDescription() const {
	return this->description; //return the description field
}

//getDoors method - returns the doors that connect to the room
const std::vector<Door>& Room::getDoors() const {
	return this->doors; //return the doors field
}

//getID method - gets the room's ID
int Room::getID() const {
	return this->id; //return the ID field
}

//addDoor method - adds a door to the room
bool Room::addDoor(Room& other, Direction doorDirection) {
	//make sure that no doors exit the room in the given direction
	//and that there is not already a connection between
	//this room and the other room
	for(const Door& d : this->doors) {
		if(d.getExitDirection() == doorDirection) {
			return false;
		} else if(other.id == d.getExitID()) {
			return false;
		}
	}

	//create the doors to connect the rooms
	Door outDoor(*this, other, doorDirection);
	Door inDoor(other, *this, outDoor.getEntryDirection());

	//add it to the door vectors
	this->doors.push_back(outDoor);
	other.doors.push_back(inDoor);

	//and return a success
	return true;
}

//hasDoorInDirection method - returns whether the room 
//has a door leading in a given direction
bool Room::hasDoorInDirection(Direction direction) const {
	//loop through the existing doors
	for(const Door& d : this->doors) {
		if(d.getExitDirection() == direction) {
			return true;
		}
	}

	//if control reaches here, then no matching direction was found
	//so we return false
	return false;
}

//output operator
namespace Orion {
	std::ostream& operator<<(std::ostream& os, const Room& r) {
		//stream out the name
		os << r.name << '\n';

		//stream out the description
		os << r.description << '\n';

		//stream out the doors
		if(r.doors.size() > 0) {
			os << "This room has exits to the: " << '\n';
			for(const Door& d : r.doors) {
				os << d << '\n';
			}
		} 

		//and return the stream
		return os;
	}
}

//end of implementation
