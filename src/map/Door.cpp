/*
 * Door.cpp
 * Implements a class that represents a door between map rooms
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Door.h"

//other include
#include "Room.h"

//namespace usage statement
using namespace Orion;

//constructor
Door::Door(const Room& entry, const Room& exit, Direction exitDirection)
	: entryID(entry.getID()), exitID(exit.getID()),
		direction(exitDirection)
{
	//no code needed
}

//destructor
Door::~Door() {
	//no code needed
}

//copy constructor
Door::Door(const Door& d)
	: entryID(d.entryID), exitID(d.exitID), direction(d.direction)
{
	//no code needed
}

//assignment operator
Door& Door::operator=(const Door& src) {
	this->entryID = src.entryID; //assign the entry ID
	this->exitID = src.exitID; //assign the exit ID
	this->direction = src.direction; //assign the direction
	return *this; //and return the instance
}

//getEntryID method - gets the ID of the entry room for the doorway
int Door::getEntryID() const {
	return this->entryID;
}

//getExitID method - gets the ID of the exit room for the doorway
int Door::getExitID() const {
	return this->exitID;
}

//getEntryDirection method - gets the direction
//of the entry room of the door
Direction Door::getEntryDirection() const {
	return this->flipDirection();
}

//getExitDirection method - gets the direction
//of the exit room of the door
Direction Door::getExitDirection() const {
	return this->direction;
}

//output operator
namespace Orion {
	std::ostream& operator<<(std::ostream& os, const Door& d) {
		//stream out the direction field
		os << nameForDirection(d.direction) << '\n';

		//and return the stream
		return os;
	}
}

//protected flipDirection method - returns the opposite Direction to
//the direction field
Direction Door::flipDirection() const {
	//calculate the opposite direction
	if(this->direction == Direction::East) {
		return Direction::West;
	} else if(this->direction == Direction::North) {
		return Direction::South;
	} else if(this->direction == Direction::Northeast) {
		return Direction::Southwest;
	} else if(this->direction == Direction::Northwest) {
		return Direction::Southeast;
	} else if(this->direction == Direction::South) {
		return Direction::North;
	} else if(this->direction == Direction::Southeast) {
		return Direction::Northwest;
	} else if(this->direction == Direction::Southwest) {
		return Direction::Northeast;
	} else { //west
		return Direction::East;
	}
}

//end of implementation
