/*
 * Map.cpp
 * Implements a class that represents a game map
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Map.h"

//other includes
#include <climits>
#include "../except/RoomIDException.h"
#include "Door.h"

//namespace usage statement
using namespace Orion;

//constructor
Map::Map(const Room& newStart)
	: start(newStart), curRoom(start), visited(),
		rooms(), roomCount(1), curID(0)
{
	//init the start room's ID
	this->start.id = this->curID++;

	//put the start room in the room vector
	this->rooms.push_back(this->start);
}

//destructor
Map::~Map() {
	//no code needed
}

//copy constructor
Map::Map(const Map& m)
	: start(m.start), curRoom(m.curRoom), visited(m.visited),
		rooms(m.rooms), roomCount(m.roomCount), curID(m.curID)
{
	//no code needed
}

//assignment operator
Map& Map::operator=(const Map& src) {
	this->start = src.start; //assign the start room
	this->curRoom = src.curRoom; //assign the current room
	this->visited = src.visited; //assign the visited map
	this->rooms = src.rooms; //assign the room map
	this->roomCount = src.roomCount; //assign the room count
	this->curID = src.curID; //assign the current ID
	return *this; //and return the instance
}

//getRoomCount method - gets the number of rooms in the map
int Map::getRoomCount() const {
	return this->roomCount; //return the room count field
}

//getCurrentRoom method - gets the current room being examined
const Room& Map::getCurrentRoom() const {
	return this->curRoom; //return the current room
}

//getStartRoom method - gets the start room of the map
const Room& Map::getStartRoom() const {
	return this->start; //return the start room
}

//move method - moves the room pointer in a given direction
bool Map::move(Direction direction) {
	//make sure that the current room has a door 
	//in the given direction
	if(!this->curRoom.hasDoorInDirection(direction)) {
		return false;
	}

	//loop and get the ID of the room to move to
	int id = -1;
	for(const Door& d : this->curRoom.getDoors()) {
		if(d.getExitDirection() == direction) {
			id = d.getExitID();
			break;
		}
	}

	//and move to that room
	if(id != -1) {
		this->curRoom = this->rooms[id];
		return true;
	} else {
		return false;
	}
}

//first addRoom method - adds a new room connected to an existing room
//selected by its ID
bool Map::addRoom(int parentID, Room& newRoom, 
			Direction doorDirection) {
	//make sure that the map actually contains the selected room
	if(!this->hasRoom(parentID)) {
		return false;
	}

	//make sure that the same room is not added twice
	if(this->hasRoom(newRoom.getID()) 
		|| this->hasRoom(newRoom.getName())) {
		return false;
	}

	//set the new room's ID
	if(this->curID < INT_MAX) {
		newRoom.id = this->curID++;
	} else {
		throw RoomIDException();
	}

	//attempt to add the room
	bool res = this->find(parentID)->addDoor(newRoom, 
							doorDirection);

	//update the room map and the room count field
	if(res) {
		this->rooms.push_back(newRoom);
		this->roomCount++;
	}

	//clear the visited map
	this->clearVisited();

	//and return the result of adding the room
	return res;
}

//second addRoom method - adds a new room connected to an existing room
//selected by its name
bool Map::addRoom(const std::string& parentName, Room& newRoom,
			Direction doorDirection) {
	//make sure that the map actually contains the selected room
	if(!this->hasRoom(parentName)) {
		return false;
	}

	//make sure that the same room is not added twice
	if(this->hasRoom(newRoom.getID()) 
		|| this->hasRoom(newRoom.getName())) {
		return false;
	}

	//set the new room's ID
	if(this->curID < INT_MAX) {
		newRoom.id = this->curID++;
	} else {
		throw RoomIDException();
	}

	//attempt to add the room
	bool res = this->find(parentName)->addDoor(newRoom,
							doorDirection);
	//update the room map and the room count field
	if(res) {
		this->rooms.push_back(newRoom);
		this->roomCount++;
	}

	//clear the visited map
	this->clearVisited();

	//and return the result of adding the room
	return res;
}

//first hasRoom method - returns whether a given room exists in the map
//selected by its ID
bool Map::hasRoom(int id) {
	//determine whether the room exists
	bool res = this->find(id) != nullptr;

	//clear the visited map
	this->clearVisited();

	//and return the result
	return res;
}

//second hasRoom method - returns whether a given room exists in the map
//selected by its name
bool Map::hasRoom(const std::string& name) {
	//determine whether the room exists
	bool res = this->find(name) != nullptr;

	//clear the visited map
	this->clearVisited();

	//and return the result
	return res;
}

//first protected find method - locates a room in the map by its ID
Room* Map::find(int id) {
	//use the first findUtil method
	return this->findUtil(id, this->start);
}

//second protected find method - locates a room in the map by its name
Room* Map::find(const std::string& name) {
	//use the second findUtil method
	return this->findUtil(name, this->start);
}

//first protected findUtil method - locates a room in the map by its ID
Room* Map::findUtil(int id, Room& root) {
	//mark the current room as visited
	this->visited[root.getID()] = true;

	//check the current room to see
	//if it's the one we're looking for
	if(root.getID() == id) {
		return &root;
	}

	//if control reaches here, then we need to search
	//the doors connecting to the current room
	for(Door& d : root.doors) {
		//get the exit room for the door
		Room& r = this->rooms.at(d.getExitID());

		//make sure that the same room doesn't
		//get checked twice
		if(this->visited[r.getID()]) {
			continue;
		}

		//check the room to see if it's the desired room
		if(r.getID() == id) {
			return &r;
		}

		//otherwise, recursively check the rooms
		//connected to the current room
		auto res = this->findUtil(id, r);
		if(res != nullptr) {
			return res;
		}

	}

	//if control reaches here, then the room
	//was not found in the map
	//so we return a nullptr
	return nullptr;
}

//second protected findUtil method - locates a room in the map by its name
Room* Map::findUtil(const std::string& name, Room& root) {
	//mark the current room as visited
	this->visited[root.getID()] = true;

	//check the current room to see
	//if it's the one we're looking for
	if(root.getName() == name) {
		return &root;
	}

	//if control reaches here, then we need to search
	//the doors connecting to the current room
	for(Door& d : root.doors) {
		//get the exit room for the door
		Room& r = this->rooms.at(d.getExitID());

		//make sure that the same room doesn't
		//get checked twice
		if(this->visited[r.getID()]) {
			continue;
		}

		//check the room to see if it's the desired room
		if(r.getName() == name) {
			return &r;
		}

		//otherwise, recursively check the rooms
		//connected to the current room
		auto res = this->findUtil(name, r);
		if(res != nullptr) {
			return res;
		}

	}

	//if control reaches here, then the room
	//was not found in the map
	//so we return a nullptr
	return nullptr;
}

//protected clearVisited method - clears the visited map
void Map::clearVisited() {
	this->visited.clear();
}

//end of implementation
