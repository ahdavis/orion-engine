/*
 * Map.h
 * Declares a class that represents a game map
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once 

//includes
#include <map>
#include <vector>
#include <string>
#include "Room.h"
#include "Direction.h"

//namespace declaration
namespace Orion {
	//class declaration
	class Map {
		//public fields and methods
		public:
			//constructor
			explicit Map(const Room& newStart);

			//destructor
			virtual ~Map();

			//copy constructor
			Map(const Map& m);

			//assignment operator
			Map& operator=(const Map& src);

			//getter methods
			
			//gets the number of rooms in the map
			int getRoomCount() const;

			//gets the current room being examined
			const Room& getCurrentRoom() const;

			//gets the start room of the map
			const Room& getStartRoom() const;

			//other methods
			
			//moves the room pointer in a given direction
			//returns whether the pointer was moved
			bool move(Direction direction);

			//adds a room to the map
			//returns whether the room was added successfully
			bool addRoom(int parentID, Room& newRoom,
					Direction doorDirection);
			bool addRoom(const std::string& parentName,
					Room& newRoom,
					Direction doorDirection);

			//returns whether a given room exists in the map
			bool hasRoom(int id);
			bool hasRoom(const std::string& name);

		//protected fields and methods
		protected:
			//methods
			
			//finds a given room in the map
			Room* find(int id);
			Room* find(const std::string& name);

			//utility methods for the find method
			Room* findUtil(int id, Room& root);
			Room* findUtil(const std::string& name,
					Room& root);

			//clears the visited map
			void clearVisited();

			//fields
			
			//the starting room of the map
			Room start;

			//the current room of the map
			Room& curRoom;

			//used to traverse the rooms
			std::map<int, bool> visited;

			//used to quickly access rooms
			std::vector<Room> rooms;

			//the number of rooms in the map
			int roomCount;

			//the current room generation ID
			int curID;
	};
}

//end of header
