/*
 * Room.h
 * Declares a class that represents a map room
 * Created on 7/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <vector>
#include <iostream>
#include "Door.h"
#include "Direction.h"

//namespace declaration
namespace Orion {
	//class declaration
	class Room {
		//public fields and methods
		public:
			//constructs with no connected rooms
			Room(const std::string& newName,
				const std::string& newDescription);

			//constructs with connected rooms
			Room(const std::string& newName,
				const std::string& newDescription,
				const std::vector<Door>& newDoors);

			//destructor
			virtual ~Room();

			//copy constructor
			Room(const Room& r);

			//assignment operator
			Room& operator=(const Room& src);

			//getter methods
			
			//gets the name of the room
			const std::string& getName() const;

			//gets the description of the room
			const std::string& getDescription() const;

			//gets the doors connecting to the room
			const std::vector<Door>& getDoors() const;

			//gets the ID of the room
			int getID() const;

			//other methods
			
			//adds a door to the room
			//returns whether the door was added successfully
			bool addDoor(Room& other,
					Direction doorDirection);

			//returns whether the room has a door
			//in a given direction
			bool hasDoorInDirection(Direction direction) const;

			//output operator
			friend std::ostream& operator<<(std::ostream& os,
							const Room& r);

		//protected fields and methods
		protected:
			//friendship declaration
			friend class Map;

			//fields
			
			//the ID of the current room being generated
			static int curID; 

			//the ID of this particular room
			int id;

			//the name of the room
			std::string name; 

			//the description of the room
			std::string description;

			//the doors that connect to the room
			std::vector<Door> doors;
	};
}

//end of header
