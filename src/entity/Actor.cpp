/*
 * Actor.cpp
 * Implements a class that represents a game actor
 * Created on 8/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "Actor.h"

//other include
#include <algorithm>

//namespace usage statement
using namespace Orion;

//default constructor
Actor::Actor()
	: Actor("No Name", "No Description", 10)
{
	//no code needed
}

//main constructor
Actor::Actor(const std::string& newName,
		const std::string& newDescription,
		int newMaxHP)
	: name(newName), description(newDescription), 
		maxHP(0), currentHP(0)
{
	this->setMaxHP(maxHP); //set the max HP
	this->currentHP = this->maxHP; //and update the current HP
}

//destructor
Actor::~Actor() {
	//no code needed
}

//copy constructor
Actor::Actor(const Actor& a)
	: name(a.name), description(a.description), 
		maxHP(a.maxHP), currentHP(a.currentHP)
{
	//no code needed
}

//assignment operator
Actor& Actor::operator=(const Actor& src) {
	this->name = src.name; //assign the name field
	this->description = src.description; //assign the description field
	this->maxHP = src.maxHP; //assign the maximum HP field
	this->currentHP = src.currentHP; //assign the current HP field
	return *this; //and return the instance
}

//getName method - gets the name of the actor
const std::string& Actor::getName() const {
	return this->name; //return the name field
}

//getDescription method - gets the description of the actor
const std::string& Actor::getDescription() const {
	return this->description; //return the description field
}

//getMaxHP method - gets the maximum HP of the actor
int Actor::getMaxHP() const {
	return this->maxHP; //return the max HP field
}

//getCurrentHP method - gets the current HP of the actor
int Actor::getCurrentHP() const {
	return this->currentHP; //return the current HP field
}

//setMaxHP method - sets the maximum HP of the actor
void Actor::setMaxHP(int newMaxHP) {
	//validate the argument and set the max HP
	if(newMaxHP < 1) {
		this->maxHP = 10;
	} else {
		this->maxHP = newMaxHP;
	}

	//and make sure that the current HP 
	//is not greater than the new max HP
	this->clampHP();
}

//damage method - damages the actor
void Actor::damage(int amt) {
	if(amt >= 0) {
		this->currentHP -= amt;
		this->clampHP();
	}
}

//heal method - heals the actor
void Actor::heal(int amt) {
	if(amt >= 0) {
		this->currentHP += amt;
		this->clampHP();
	}
}

//interact method - causes the actor to interact with another
void Actor::interact(Actor& other) {
	//do nothing
}

//friend methods
namespace Orion {
	//serialization operator
	std::ostream& operator<<(const Actor& a, std::ostream& os) {
		//stream out the fields
		os << a.name << '\n';
		os << a.description << '\n';
		os << a.maxHP << '\n';
		os << a.currentHP << '\n';

		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(Actor& a, std::istream& is) {
		std::getline(is, a.name); //read in the name
		std::getline(is, a.description); //read the description
		is >> a.maxHP; //read in the max HP
		is >> a.currentHP; //read in the current HP
		return is; //and return the stream
	}
}

//protected clampHP method - corrects the current HP of the actor
void Actor::clampHP() {
	//correct the current HP
	this->currentHP = std::clamp(this->currentHP, 0, this->maxHP);
}

//end of implementation
