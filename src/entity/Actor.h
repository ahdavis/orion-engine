/*
 * Actor.h
 * Declares a class that represents a game actor
 * Created on 8/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <string>

//namespace declaration
namespace Orion {
	//class declaration
	class Actor {
		//public fields and methods
		public:
			//default constructor
			//use this for serialization
			Actor();

			//main constructor
			Actor(const std::string& newName,
				const std::string& newDescription,
				int newMaxHP);

			//destructor
			virtual ~Actor();

			//copy constructor
			Actor(const Actor& a);

			//assignment operator
			Actor& operator=(const Actor& src);

			//getter methods
			
			//gets the name of the actor
			const std::string& getName() const;

			//gets the description of the actor
			const std::string& getDescription() const;

			//gets the maximum HP of the actor
			int getMaxHP() const;

			//gets the current HP of the actor
			int getCurrentHP() const;

			//setter method
			
			//sets the maximum HP of the actor
			//if the value is below one,
			//then the maximum HP will be
			//set to 10
			void setMaxHP(int newMaxHP);

			//other methods
			
			//damages the actor by a given amount
			//if the value is negative, nothing
			//will happen
			void damage(int amt);

			//heals the actor by a given amount
			//if the value is negative, nothing
			//will happen
			void heal(int amt);

			//causes the actor to interact with another
			virtual void interact(Actor& other);

			//serialization operator
			friend std::ostream& operator<<(const Actor& a,
							std::ostream& os);

			//deserialization operator
			friend std::istream& operator>>(Actor& a,
							std::istream& is);

		//protected fields and methods
		protected:
			//method
			void clampHP(); //corrects the current HP field

			//fields
			std::string name; //the name of the actor
			std::string description; //the actor's description
			int maxHP; //the maximum HP of the actor
			int currentHP; //the current HP of the actor
	};
}

//end of header
