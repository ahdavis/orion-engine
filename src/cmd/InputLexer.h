/*
 * InputLexer.h
 * Declares a class that lexes user input
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <vector>
#include "InputRule.h"
#include "InputToken.h"

//namespace declaration
namespace Orion {
	//class declaration
	class InputLexer {
		//public fields and methods
		public:
			//constructs with an empty rule array
			explicit InputLexer(const std::string& newInput);

			//constructs with a rule array
			InputLexer(const std::string& newInput,
				const std::vector<InputRule>& newRules);

			//destructor
			virtual ~InputLexer();

			//copy constructor
			InputLexer(const InputLexer& il);

			//assignment operator
			InputLexer& operator=(const InputLexer& src);

			//sets the input string
			void setInput(const std::string& newInput);

			//gets the next token consumed from the input
			InputToken nextToken();

			//adds a rule to the lexer
			//returns whether the rule
			//was added successfully
			bool addRule(const InputRule& rule);

		//protected fields and methods
		protected:
			//method
			
			//splits the input string on spaces
			void splitInput();

			//fields
			
			//the input string
			std::string input;
			
			//the words in the input string
			std::vector<std::string> words;

			//the rules that the lexer checks against
			std::vector<InputRule> rules;

			//the current position in the word vector
			//that is being lexed
			int pos;
	};
}

//end of header
