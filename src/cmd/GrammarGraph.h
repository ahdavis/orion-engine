/*
 * GrammarGraph.h
 * Declares a class that represents a grammar graph
 * Created on 7/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "GrammarNode.h"
#include <string>
#include <map>
#include <vector>

//namespace declaration
namespace Orion {
	//class declaration
	class GrammarGraph {
		//public fields and methods
		public:
			//constructor
			explicit GrammarGraph(const std::string& rootName);

			//destructor
			virtual ~GrammarGraph();

			//copy constructor
			GrammarGraph(const GrammarGraph& gg);

			//assignment operator
			GrammarGraph& operator=(const GrammarGraph& src);

			//getter method
			
			//gets the root element of the graph
			const GrammarNode& getRoot() const;

			//other methods
			
			//adds a node to the graph and connects
			//it to a pre-existing node
			//returns whether the node was
			//added successfully
			bool addNode(const std::string& parentName,
					const std::string& newNodeName);
			
			//returns whether one node follows another
			//in the graph
			bool follows(const std::string& first,
					const std::string& second);

			//returns whether a node is present in the graph
			bool hasNode(const std::string& name);

		//protected fields and methods
		protected:
			//methods
			
			//finds a given node in the graph
			GrammarNode* find(const std::string& name,
						GrammarNode& root);

			//clears the visited map
			void clearVisited();

			//fields
			
			//the root node of the graph
			GrammarNode root; 

			//stores whether a graph node has been visited
			std::map<std::string, bool> visited;
	};
}

//end of header
