/*
 * InputList.cpp
 * Implements a class that represents an input list
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputList.h"

//namespace usage statement
using namespace Orion;

//constructor
InputList::InputList()
	: count(0), list() //init the fields
{
	//no code needed
}

//destructor
InputList::~InputList() {
	//no code needed
}

//copy constructor
InputList::InputList(const InputList& il)
	: count(il.count), list(il.list) //copy the fields
{
	//no code needed
}

//assignment operator
InputList& InputList::operator=(const InputList& src) {
	this->count = src.count; //assign the count field
	this->list = src.list; //assign the list field
	//and return the instance
	return *this;
}

//length method - gets the length of the list
int InputList::length() const {
	return this->count; //return the count field
}

//add method - adds an item to the list
void InputList::add(const InputToken& item) {
	//insert the token
	this->list.push_back(item); //add the item to the list
	//and increment the count field
	this->count++;
}


//get method - gets the list item at a given offset
const InputToken& InputList::get(int pos) const {
	return this->list.at(pos); //return the item
}

//isEmpty method - returns whether the list is empty
bool InputList::isEmpty() const {
	return this->count == 0;
}

//begin method - returns an iterator for the beginning of the list
InputIterator InputList::begin() const {
	return InputIterator(this, 0);
}

//end method - returns an iterator for the end of the list
InputIterator InputList::end() const {
	return InputIterator(this, this->count);
}

//end of implementation
