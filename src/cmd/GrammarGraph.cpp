/*
 * GrammarGraph.cpp
 * Implements a class that represents a grammar graph
 * Created on 7/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "GrammarGraph.h"

//namespace usage statement
using namespace Orion;

//constructor
GrammarGraph::GrammarGraph(const std::string& rootName)
	: root(rootName), visited() //init the fields
{
	//no code needed
}

//destructor
GrammarGraph::~GrammarGraph() {
	//no code needed
}

//copy constructor
GrammarGraph::GrammarGraph(const GrammarGraph& gg)
	: root(gg.root), visited(gg.visited) //copy the fields
{
	//no code needed
}

//assignment operator
GrammarGraph& GrammarGraph::operator=(const GrammarGraph& src) {
	this->root = src.root; //assign the root node
	this->visited = src.visited; //assign the visited map
	return *this; //and return the instance
}

//getRoot method - gets the root node of the graph
const GrammarNode& GrammarGraph::getRoot() const {
	return this->root; //return the root node
}

//addNode method - adds a node to the graph and returns
//whether it was successfully added
bool GrammarGraph::addNode(const std::string& parentName,
				const std::string& newNodeName) {
	//make sure the parent node exists
	if(!this->hasNode(parentName)) {
		return false;
	}

	//create the node to add to the graph
	GrammarNode newNode(newNodeName);

	//add it to the parent node and get the result
	bool res = this->find(parentName, 
				this->root)->addConnection(newNode);

	//clear the visited map
	this->clearVisited();

	//and return the result of the add operation
	return res;
}

//follows method - returns whether one node follows another in the graph
bool GrammarGraph::follows(const std::string& first,
				const std::string& second) {
	//make sure the first node is present in the graph
	if(this->hasNode(first)) { //if the first node is present
		//then get whether first connects to second
		bool ret = this->find(first, 
					this->root)->connectsTo(second);

		//clear the visited map
		this->clearVisited();

		//and return the result
		return ret;
	} else { //if the first node is not present
		//then return false
		return false;
	}
}

//hasNode method - returns whether a node exists in the graph
bool GrammarGraph::hasNode(const std::string& name) {
	//attempt to find the node
	bool ret = this->find(name, this->root) != nullptr;

	//clear the visited map
	this->clearVisited();

	//and return whether the node was found
	return ret;
}

//protected find method - locates a node in the graph
GrammarNode* GrammarGraph::find(const std::string& name,
					GrammarNode& root) {
	//mark the current node as visited
	this->visited[root.getName()] = true;

	//check the current node to see if it is the
	//desired node
	if(root.getName() == name) {
		return &root;
	}

	//if control reaches here, then we need to search
	//the connections of the current node
	for(GrammarNode& gn : root.connections) {
		//allow circular connections
		if(gn.getName() == root.getName()) {
			return &gn;
		}

		//make sure that the current connection
		//has not been visited
		if(this->visited[gn.getName()]) {
			continue;
		}

		//check the current connection to see
		//if it matches the desired node
		if(gn.getName() == name) {
			return &gn;
		}

		//otherwise, recursively check the
		//current connection
		auto ret = this->find(name, gn);
		if(ret != nullptr) {
			return ret;
		}
	}

	//if control reaches here, then the node was not found
	//so we return a nullptr
	return nullptr;
}

//protected clearVisited method - clears the visited map
void GrammarGraph::clearVisited() {
	this->visited.clear(); //clear the visited map
}

//end of implementation
