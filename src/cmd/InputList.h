/*
 * InputList.h
 * Declares a class that represents an input list
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <vector>
#include "InputToken.h"
#include "InputIterator.h"

//namespace declaration
namespace Orion {
	//class declaration
	class InputList {
		//public fields and methods
		public:
			//constructor
			InputList();

			//destructor
			virtual ~InputList();

			//copy constructor
			InputList(const InputList& il);

			//assignment operator
			InputList& operator=(const InputList& src);

			//returns the length of the list
			int length() const;

			//adds an item to the list
			void add(const InputToken& item);

			//gets the item at a given offset
			//into the list
			const InputToken& get(int pos) const;

			//gets whether the list is empty
			bool isEmpty() const;

			//returns an iterator for the
			//beginning of the list
			InputIterator begin() const;

			//returns an iterator for the
			//end of the list
			InputIterator end() const;

		//protected fields and methods
		protected:
			//fields
			int count; //the number of nodes in the list
			std::vector<InputToken> list; //the list itself
	};
}

//end of header
