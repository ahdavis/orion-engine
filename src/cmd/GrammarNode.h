/*
 * GrammarNode.h
 * Declares a class that represents a node in a grammar graph
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <vector>

//namespace declaration
namespace Orion {
	//class declaration
	class GrammarNode {
		//public fields and methods
		public:
			//constructs with no connected nodes
			explicit GrammarNode(const std::string& newName);

			//constructs with an array of connected nodes
			GrammarNode(const std::string& newName,
				const std::vector<GrammarNode>& newCnxns);

			//destructor
			virtual ~GrammarNode();

			//copy constructor
			GrammarNode(const GrammarNode& gn);

			//assignment operator
			GrammarNode& operator=(const GrammarNode& src);

			//getter methods
			
			//gets the name of the node
			const std::string& getName() const;

			//gets the connections of the node
			const std::vector<GrammarNode>& getCnxns() const;

			//other methods
			
			//gets whether the node connects to another
			bool connectsTo(const std::string& name) const;

			//adds a connection to the node
			//returns whether the connection
			//was added successfully
			bool addConnection(const GrammarNode& cnxn);

		//protected fields and methods
		protected:
			//friendship declaration
			friend class GrammarGraph;

			//fields
			
			//the name of the node
			std::string name;

			//the nodes that the node connects to
			std::vector<GrammarNode> connections; 
	};
}

//end of header
