/*
 * InputRule.cpp
 * Implements a class that represents a rule for differentiating user input
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputRule.h"

//namespace usage statement
using namespace Orion;

//constructor
InputRule::InputRule(const std::string& newName, 
			const std::string& newPattern)
	: name(newName), pattern(newPattern), matcher(newPattern)
{
	//no code needed
}

//destructor
InputRule::~InputRule() {
	//no code needed
}

//copy constructor
InputRule::InputRule(const InputRule& ir)
	: name(ir.name), pattern(ir.pattern), matcher(ir.matcher)
{
	//no code needed
}

//assignment operator
InputRule& InputRule::operator=(const InputRule& src) {
	this->name = src.name; //assign the name field
	this->pattern = src.pattern; //assign the pattern field
	this->matcher = src.matcher; //assign the matcher field
	return *this; //and return the instance
}

//getName method - gets the name of the rule
const std::string& InputRule::getName() const {
	return this->name; //return the name field
}

//getPattern method - gets the pattern of the rule
const std::string& InputRule::getPattern() const {
	return this->pattern; //return the pattern field
}

//setPattern method - sets the pattern of the rule
void InputRule::setPattern(const std::string& newPattern) {
	this->pattern = newPattern; //update the pattern field
	this->matcher = std::regex(this->pattern); //and update the regex
}

//matches method - determines whether the rule matches a given string
bool InputRule::matches(const std::string& str) const {
	return std::regex_match(str, this->matcher); 
}

//generateToken method - generates a token from the rule
InputToken InputRule::generateToken(const std::string& str) const {
	//make sure that the string matches the rule
	if(this->matches(str)) { //if the string matches
		//then return the token
		return InputToken(this->name, str);
	} else { //if the string does not match
		//then return an error token
		return InputToken::ERROR;
	}
}

//end of file
