/*
 * InputRule.h
 * Declares a class that represents a rule for differentiating user input
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <regex>
#include "InputToken.h"

//namespace declaration
namespace Orion {
	//class declaration
	class InputRule {
		//public fields and methods
		public:
			//constructor
			InputRule(const std::string& newName,
					const std::string& newPattern);

			//destructor
			virtual ~InputRule();

			//copy constructor
			InputRule(const InputRule& ir);

			//assignment operator
			InputRule& operator=(const InputRule& src);

			//getter methods
			
			//gets the name of the rule
			const std::string& getName() const;

			//gets the pattern of the rule
			const std::string& getPattern() const;

			//setter method
			
			//sets the pattern of the rule
			void setPattern(const std::string& newPattern);

			//other methods
			
			//determines whether the rule
			//matches a given string
			bool matches(const std::string& str) const;

			//generates a token from a string
			//returns a token with type "ERROR"
			//if the given string does not match the rule
			InputToken generateToken(
					const std::string& str) const;

		//protected fields and methods
		protected:
			//fields
			std::string name; //the name of the rule
			std::string pattern; //the pattern of the rule
			std::regex matcher; //used to match strings
	};
}

//end of header
