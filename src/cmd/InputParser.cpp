/*
 * InputParser.cpp
 * Implements a class that parses user commands
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputParser.h"

//other include
#include "../except/InputParserException.h"

//namespace usage statement
using namespace Orion;

//constructor
InputParser::InputParser(const InputLexer& newLexer,
				const GrammarGraph& newGrammar)
	: lexer(newLexer), grammar(newGrammar), 
		curToken(InputToken::ERROR)
{
	//get the first token
	this->curToken = this->lexer.nextToken();
}

//destructor
InputParser::~InputParser() {
	//no code needed
}

//copy constructor
InputParser::InputParser(const InputParser& ip)
	: lexer(ip.lexer), grammar(ip.grammar), curToken(ip.curToken)
{
	//no code needed
}

//assignment operator
InputParser& InputParser::operator=(const InputParser& src) {
	this->lexer = src.lexer; //assign the lexer
	this->grammar = src.grammar; //assign the grammar
	this->curToken = src.curToken; //assign the current token
	return *this; //and return the instance
}

//parse method - parses input and generates a list of verified tokens
InputList InputParser::parse() {
	//declare the return value
	InputList ret;

	//add the first token
	ret.add(this->curToken);

	//loop and parse commands
	while(this->curToken != InputToken::EOL) {
		//get the next token
		InputToken newToken = this->lexer.nextToken();

		//check if for EOL
		if(newToken == InputToken::EOL) {
			break;
		}

		//verify it
		if(!this->grammar.follows(this->curToken.getType(),
					newToken.getType())) {
			throw InputParserException(this->curToken,
							newToken);
		}

		//make it the new current token
		this->curToken = newToken;

		//and add it to the list
		ret.add(this->curToken);
	}

	//and return the populated list
	return ret;
}

//end of implementation
