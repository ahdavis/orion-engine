/*
 * GrammarNode.cpp
 * Implements a class that represents a node in a grammar graph
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "GrammarNode.h"

//namespace usage statement
using namespace Orion;

//first constructor - constructs without any connections
GrammarNode::GrammarNode(const std::string& newName)
	: GrammarNode(newName, std::vector<GrammarNode>())
{
	//no code needed
}

//second constructor - constructs from an array of child connections
GrammarNode::GrammarNode(const std::string& newName,
			const std::vector<GrammarNode>& newCnxns)
	: name(newName), connections()
{
	//add the child connections
	for(const GrammarNode& gn : newCnxns) {
		this->addConnection(gn);
	}
}

//destructor
GrammarNode::~GrammarNode() {
	//no code needed
}

//copy constructor
GrammarNode::GrammarNode(const GrammarNode& gn)
	: name(gn.name), connections(gn.connections)
{
	//no code needed
}

//assignment operator
GrammarNode& GrammarNode::operator=(const GrammarNode& src) {
	this->name = src.name; //assign the name field
	this->connections = src.connections; //assign the connections field
	return *this; //and return the instance
}

//getName method - gets the name of the node
const std::string& GrammarNode::getName() const {
	return this->name; //return the name field
}

//getCnxns method - gets the connection array for the node
const std::vector<GrammarNode>& GrammarNode::getCnxns() const {
	return this->connections; //return the connection array
}

//connectsTo method - returns whether this node connects to another
bool GrammarNode::connectsTo(const std::string& name) const {
	//loop and check connections
	for(const GrammarNode& gn : this->connections) {
		if(gn.name == name) {
			return true;
		}
	}

	//if control reaches here, then no connection was found
	//so we return false
	return false;
}

//addConnection method - adds a connection to the node
//and returns whether the connection was successfully added
bool GrammarNode::addConnection(const GrammarNode& cnxn) {
	//make sure that the new connection does not
	//share a name with any existing connections
	for(const GrammarNode& gn : this->connections) {
		if(gn.name == cnxn.name) {
			return false;
		}
	}

	//if control reaches here, then the connection is unique
	//so we add the connection and return a success
	this->connections.push_back(cnxn);
	return true;
}

//end of implementation
