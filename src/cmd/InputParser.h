/*
 * InputParser.h
 * Declares a class that parses user commands
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "InputLexer.h"
#include "InputToken.h"
#include "InputList.h"
#include "GrammarGraph.h"

//namespace declaration
namespace Orion {
	//class declaration
	class InputParser {
		//public fields and methods
		public:
			//constructor
			InputParser(const InputLexer& newLexer,
					const GrammarGraph& newGrammar);

			//destructor
			virtual ~InputParser();

			//copy constructor
			InputParser(const InputParser& ip);

			//assignment operator
			InputParser& operator=(const InputParser& src);

			//parses input and returns a list of 
			//verified tokens
			InputList parse();

		//protected fields and methods
		protected:
			//fields
			InputLexer lexer; //lexes the user input
			GrammarGraph grammar; //the command grammar
			InputToken curToken; //the current token
	};
}	

//end of header
