/*
 * InputLexer.cpp
 * Implements a class that lexes user input
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputLexer.h"

//other includes
#include <sstream>
#include <algorithm>
#include <iterator>
#include "../except/InputLexerException.h"

//namespace usage statement
using namespace Orion;

//first constructor - constructs with an empty rule array
InputLexer::InputLexer(const std::string& newInput)
	: InputLexer(newInput, std::vector<InputRule>())
{
	//no code needed
}

//second constructor - constructs with a rule array
InputLexer::InputLexer(const std::string& newInput,
			const std::vector<InputRule>& newRules)
	: input(), words(), rules(), pos() //zero the fields
{
	//set the input string and populate the words array
	this->setInput(newInput);

	//and populate the rules array
	for(const InputRule& rule : newRules) {
		this->addRule(rule);
	}
}

//destructor
InputLexer::~InputLexer() {
	//no code needed
}

//copy constructor
InputLexer::InputLexer(const InputLexer& il)
	: input(il.input), words(il.words), rules(il.rules), pos(il.pos)
{
	//no code needed
}

//assignment operator
InputLexer& InputLexer::operator=(const InputLexer& src) {
	this->input = src.input; //assign the input string
	this->words = src.words; //assign the words array
	this->rules = src.rules; //assign the rules array
	this->pos = src.pos; //assign the lexer position
	return *this; //and return the instance
}

//setInput method - sets the input string
void InputLexer::setInput(const std::string& newInput) {
	this->input = newInput; //update the input field
	this->splitInput(); //split the input string on spaces
	this->pos = 0; //and restart lexing from the start of the input
}

//nextToken method - gets the next token consumed from the input
InputToken InputLexer::nextToken() {
	//make sure that the end of the input
	//has not been reached
	if(this->pos >= this->words.size()) {
		return InputToken::EOL;
	}

	//get the current word being lexed
	std::string curWord = this->words[this->pos++];

	//loop and attempt to match it to a rule
	for(const InputRule& rule : this->rules) {
		if(rule.matches(curWord)) {
			return rule.generateToken(curWord);
		}
	}

	//if control reaches here, then no rule matched
	//the current word, so we throw an exception
	throw InputLexerException(curWord);
}

//addRule method - adds a rule to the lexer
//and returns whether it was added successfully
bool InputLexer::addRule(const InputRule& rule) {
	//make sure an identical rule is not already
	//in the array
	for(const InputRule& r : this->rules) {
		if((r.getName() == rule.getName()) ||
			(r.getPattern() == rule.getPattern())) {
			return false;
		}
	}

	//add the rule to the array
	this->rules.push_back(rule);

	//and return a success
	return true;
}

//protected splitInput method - splits the input string on spaces
//and populates the words array
void InputLexer::splitInput() {
	//get a stringstream to parse the input
	std::istringstream iss(this->input);

	//and copy the input into the vector, splitting on spaces
	std::copy(std::istream_iterator<std::string>(iss),
			std::istream_iterator<std::string>(),
			std::back_inserter(this->words));
}

//end of implementation
