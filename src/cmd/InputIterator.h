/*
 * InputIterator.h
 * Declares a class that iterates through an input list
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include "InputToken.h"

//namespace declaration
namespace Orion {
	//forward declare the InputList class
	class InputList;

	//class declaration
	class InputIterator final {
		//public fields and methods
		public:
			//constructor
			InputIterator(const InputList* newList, 
					int newPos);

			//destructor
			~InputIterator();

			//copy constructor
			InputIterator(const InputIterator& ii);

			//assignment operator
			InputIterator& operator=(const InputIterator& src);

			//inequality operator
			bool operator!=(const InputIterator& other) const;

			//dereference operator
			const InputToken& operator*() const;

			//prefix increment operator
			InputIterator& operator++();

			//postfix increment operator
			InputIterator operator++(int unused);

		//private fields and methods
		private:
			//fields
			int pos; //the current position in the list
			const InputList* list; //the list being iterated
	};
}

//end of header
