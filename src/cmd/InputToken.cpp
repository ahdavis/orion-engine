/*
 * InputToken.cpp
 * Implements a class that represents a unit of user input
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputToken.h"

//namespace usage statement
using namespace Orion;

//static field initializations
const InputToken InputToken::EOL = InputToken("EOL", "");
const InputToken InputToken::ERROR = InputToken("ERROR", "");

//constructor
InputToken::InputToken(const std::string& newType,
			const std::string& newValue)
	: type(newType), value(newValue) //init the fields
{
	//no code needed
}

//destructor
InputToken::~InputToken() {
	//no code needed
}

//copy constructor
InputToken::InputToken(const InputToken& it)
	: type(it.type), value(it.value) //copy the fields
{
	//no code needed
}

//assignment operator
InputToken& InputToken::operator=(const InputToken& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//overloaded equality operator
bool InputToken::operator==(const InputToken& other) const {
	//compare the fields
	return (this->type == other.type) && 
		(this->value == other.value);
}

//overloaded inequality operator
bool InputToken::operator!=(const InputToken& other) const {
	bool isEqual = *this == other; //get whether the objects are equal
	return !isEqual; //and return the opposite of that value
}

//getType method - gets the type of the token
const std::string& InputToken::getType() const {
	return this->type; //return the type field
}

//getValue method - gets the value of the token
const std::string& InputToken::getValue() const {
	return this->value; //return the value field
}

//end of implementation
