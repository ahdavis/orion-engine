/*
 * InputIterator.cpp
 * Implements a class that iterates through an input list
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the class header
#include "InputIterator.h"

//include the InputList header
#include "InputList.h"

//namespace usage statement
using namespace Orion;

//constructor
InputIterator::InputIterator(const InputList* newList, int newPos)
	: pos(newPos), list(newList) //init the fields
{
	//no code needed
}

//destructor
InputIterator::~InputIterator() {
	//no code needed
}

//copy constructor
InputIterator::InputIterator(const InputIterator& ii)
	: pos(ii.pos), list(ii.list) //copy the fields
{
	//no code needed
}

//assignment operator
InputIterator& InputIterator::operator=(const InputIterator& src) {
	this->pos = src.pos; //assign the position field
	this->list = src.list; //assign the list field
	return *this; //and return the instance
}

//inequality operator
bool InputIterator::operator!=(const InputIterator& other) const {
	return this->pos != other.pos; //compare the positions
}

//dereference operator
const InputToken& InputIterator::operator*() const {
	return this->list->get(this->pos); //return the current list item
}

//prefix increment operator
InputIterator& InputIterator::operator++() {
	this->pos++; //increment the position
	return *this; //and return the instance
}

//postfix increment operator
InputIterator InputIterator::operator++(int unused) {
	InputIterator temp = *this; //get a reference to the current state
	++*this; //call the other increment operator
	return temp; //and return the reference
}

//end of implementation
