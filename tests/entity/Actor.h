/*
 * Actor.h
 * Defines unit tests for the Actor class
 * Created on 8/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/entity/Actor.h"
#include <gtest/gtest.h>

//test class
class ActorTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		ActorTest()
			: defaultActor(), 
				normalActor("Orc", "A nasty orc", 20),
				badHPActor("Bad HP", "Tests bad HP", 0)
		{
			//no code needed
		}

		//destructor
		~ActorTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::Actor defaultActor;
		Orion::Actor normalActor;
		Orion::Actor badHPActor;
};

//this test checks that the default max HP value is 10
TEST_F(ActorTest, DefaultHPIs10) {
	EXPECT_EQ(defaultActor.getMaxHP(), 10);
}

//this test checks that an invalid max HP value defaults to 10
TEST_F(ActorTest, BadHPIs10) {
	EXPECT_EQ(badHPActor.getMaxHP(), 10);
	badHPActor.setMaxHP(-1);
	EXPECT_EQ(badHPActor.getMaxHP(), 10);
}

//this test checks that a new actor has equal current HP and max HP
TEST_F(ActorTest, HPValuesAreEqual) {
	EXPECT_EQ(normalActor.getMaxHP(), normalActor.getCurrentHP());
}

//this test checks that an actor's current HP cannot be negative
TEST_F(ActorTest, HPNotNegative) {
	normalActor.damage(1000);
	EXPECT_EQ(normalActor.getCurrentHP(), 0);
}

//this test checks that you cannot call Actor::damage()
//with a negative argument
TEST_F(ActorTest, NegativeDamage) {
	int hp = normalActor.getCurrentHP();
	normalActor.damage(-20);
	EXPECT_EQ(normalActor.getCurrentHP(), hp);
}

//this test checks that an actor cannot be healed past max HP
TEST_F(ActorTest, HealPastMax) {
	normalActor.heal(1000);
	EXPECT_LE(normalActor.getCurrentHP(), normalActor.getMaxHP());
}

//this test checks that you cannot call Actor::heal()
//with a negative argument
TEST_F(ActorTest, NegativeHeal) {
	int hp = normalActor.getCurrentHP();
	normalActor.heal(-20);
	EXPECT_EQ(normalActor.getCurrentHP(), hp);
}

//end of tests
