/*
 * InputLexer.h
 * Defines unit tests for the InputLexer class
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/InputLexer.h"
#include "../../src/cmd/InputRule.h"
#include "../../src/cmd/InputToken.h"
#include <gtest/gtest.h>

//test class
class InputLexerTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		InputLexerTest()
			: lexer("Hello World 1213 1996!")
		{
			//no code needed
		}

		//destructor
		~InputLexerTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//add rules to the lexer
			lexer.addRule(Orion::InputRule("STR", 
						"[a-zA-Z]+"));
			lexer.addRule(Orion::InputRule("INT", 
						"[0-9]+"));
			lexer.addRule(Orion::InputRule("FACT", 
						"[0-9]+!"));
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::InputLexer lexer;
};

//This test verifies that a rule with the same name
//cannot be added to the lexer
TEST_F(InputLexerTest, SameNameRule) {
	Orion::InputRule rule("INT", "[0-9]"); //create a rule to add
	bool res = lexer.addRule(rule); //add the rule (this should fail)
	EXPECT_EQ(res, false); //and execute the test
}

//This test verifies that a rule with the same pattern
//cannot be added to the lexer
TEST_F(InputLexerTest, SamePatternRule) {
	Orion::InputRule rule("EXCLM", "[0-9]+!"); //create a rule to add
	bool res = lexer.addRule(rule); //add the rule (this should fail)
	EXPECT_EQ(res, false); //and execute the test
}

//This test verifies that a completely unique rule
//can be added to the lexer
TEST_F(InputLexerTest, UniqueRuleAdd) {
	Orion::InputRule rule("EXCLM", "!!!"); //create a rule to add
	bool res = lexer.addRule(rule); //add the rule (this should work)
	EXPECT_EQ(res, true); //and execute the test
}

//This test checks the lexing process
TEST_F(InputLexerTest, LexingProcess) {
	Orion::InputToken t = lexer.nextToken(); //get the first token
	EXPECT_EQ(t, Orion::InputToken("STR", "Hello")); //verify it
	t = lexer.nextToken(); //get the second token
	EXPECT_EQ(t, Orion::InputToken("STR", "World")); //verify it
	t = lexer.nextToken(); //get the third token
	EXPECT_EQ(t, Orion::InputToken("INT", "1213")); //verify it
	t = lexer.nextToken(); //get the fourth token
	EXPECT_EQ(t, Orion::InputToken("FACT", "1996!")); //verify it
	t = lexer.nextToken(); //get the last token
	EXPECT_EQ(t, Orion::InputToken::EOL); //and verify it
}

//end of tests
