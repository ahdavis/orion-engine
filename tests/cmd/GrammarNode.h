/*
 * GrammarNode.h
 * Defines unit tests for the GrammarNode class
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/GrammarNode.h"
#include <gtest/gtest.h>

//test class
class GrammarNodeTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		GrammarNodeTest()
			: startNode("Start Node")
		{
			//no code needed
		}

		//destructor
		~GrammarNodeTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//create connection nodes
			Orion::GrammarNode cnxn1("Cnxn 1");
			Orion::GrammarNode cnxn2("Cnxn 2");

			//and connect them to the start node
			startNode.addConnection(cnxn1);
			startNode.addConnection(cnxn2);
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::GrammarNode startNode;
};

//This test verifies that non-unique nodes cannot be connected
TEST_F(GrammarNodeTest, NonUniqueConnection) {
	Orion::GrammarNode nonUnique("Cnxn 1"); //a non-unique node
	bool res = startNode.addConnection(nonUnique); //add the node
	EXPECT_EQ(res, false); //and execute the test
}

//This test verifies that unique nodes can be connected
TEST_F(GrammarNodeTest, UniqueConnection) {
	Orion::GrammarNode unique("Cnxn 3"); //a unique node
	bool res = startNode.addConnection(unique); //add the node
	EXPECT_EQ(res, true); //and execute the test
}

//This test verifies the connectsTo method
TEST_F(GrammarNodeTest, ConnectsTo) {
	//get connection stati
	bool res1 = startNode.connectsTo("Cnxn 1"); //should be true
	bool res2 = startNode.connectsTo("Cnxn 3"); //should be false
	
	//and execute the test
	EXPECT_EQ(res1, true);
	EXPECT_EQ(res2, false);
}

//end of tests
