/*
 * InputParser.h
 * Defines unit tests for the InputParser class
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/GrammarGraph.h"
#include "../../src/cmd/InputLexer.h"
#include "../../src/cmd/InputRule.h"
#include "../../src/cmd/InputParser.h"
#include "../../src/cmd/InputList.h"
#include <gtest/gtest.h>

//test class
class InputParserTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		InputParserTest()
			: parser(nullptr)
		{
			//no code needed
		}

		//destructor
		~InputParserTest() override {
			delete parser;
			parser = nullptr;
		}

		//Sets up the test suite
		void SetUp() override {
			//create a lexer
			Orion::InputLexer lexer("Hello World 1213 Hi 13");

			//add rules to it
			lexer.addRule(Orion::InputRule("STR", 
							"[a-zA-Z]+"));
			lexer.addRule(Orion::InputRule("INT",
							"[0-9]+"));
			lexer.addRule(Orion::InputRule("MONEY",
							"\\$[0-9]+"));

			//create a grammar graph
			Orion::GrammarGraph grammar("STR");
			
			//populate it
			grammar.addNode("STR", "STR");
			grammar.addNode("STR", "INT");
			grammar.addNode("INT", "STR");

			//and create the parser
			parser = new Orion::InputParser(lexer,
							grammar);
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::InputParser* parser;
};

//this test checks the parse method
TEST_F(InputParserTest, Parse) {
	Orion::InputList tokens = parser->parse();
	auto iter = tokens.begin();
	EXPECT_EQ(*iter, Orion::InputToken("STR", "Hello"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("STR", "World"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("INT", "1213"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("STR", "Hi"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("INT", "13"));
	iter++;
	EXPECT_EQ(iter != tokens.end(), false);
}

//end of tests
