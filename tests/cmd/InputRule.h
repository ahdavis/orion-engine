/*
 * InputRule.h
 * Defines unit tests for the InputRule class
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/InputToken.h"
#include "../../src/cmd/InputRule.h"
#include <gtest/gtest.h>
#include <string>

//test class
class InputRuleTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		InputRuleTest()
			: rule("INT", "[0-9]+")
		{
			//no code needed
		}

		//destructor
		~InputRuleTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::InputRule rule; //the rule to match
};

//This test checks correctly matching a string
TEST_F(InputRuleTest, Matches) {
	EXPECT_EQ(rule.matches("1996"), true);
}

//This test checks incorrectly matching a string
TEST_F(InputRuleTest, DoesNotMatch) {
	EXPECT_EQ(rule.matches("Hello, World!"), false);
}

//This test checks token generation for a matching string
TEST_F(InputRuleTest, TokenGenMatches) {
	//create test variables
	std::string text = "420"; //the text to match
	Orion::InputToken other(rule.getName(), text); //the test token

	//get the generated token
	Orion::InputToken gen = rule.generateToken(text);

	//and execute the test
	EXPECT_EQ(gen, other);
}

//This test checks that an error token is generated 
//when a string does not match
TEST_F(InputRuleTest, TokenGenError) {
	//create the test string
	std::string text = "Hello, World";

	//get the generated token
	Orion::InputToken gen = rule.generateToken(text);

	//and execute the test
	EXPECT_EQ(gen.getType(), "ERROR");
}

//end of tests
