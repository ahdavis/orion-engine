/*
 * InputToken.h
 * Defines unit tests for the InputToken class
 * Created on 7/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/InputToken.h"
#include <gtest/gtest.h>

//test class
class InputTokenTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		InputTokenTest()
			: eolToken(Orion::InputToken::EOL),
				errToken(Orion::InputToken::ERROR),
				hasValue("INT", "42")
		{
			//no code needed
		}

		//destructor
		~InputTokenTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::InputToken eolToken;
		Orion::InputToken errToken;
		Orion::InputToken hasValue;
};

//This test makes sure that the value of an 
//error token defaults to an empty string
TEST_F(InputTokenTest, ErrorValueIsEmpty) {
	EXPECT_EQ(errToken.getValue(), "");
}

//This test makes sure that the type of an
//error token is "ERROR"
TEST_F(InputTokenTest, ErrorType) {
	EXPECT_EQ(errToken.getType(), "ERROR");
}

//This test makes sure that the value of an
//EOL token defaults to an empty string
TEST_F(InputTokenTest, EOLValueIsEmpty) {
	EXPECT_EQ(eolToken.getValue(), "");
}

//This test makes sure that the type of an
//EOL token is "EOL"
TEST_F(InputTokenTest, EOLType) {
	EXPECT_EQ(eolToken.getType(), "EOL");
}

//This test checks the equality operator
TEST_F(InputTokenTest, EqualityOperator) {
	EXPECT_EQ(hasValue == hasValue, true);
}

//This test checks the inequality operator
TEST_F(InputTokenTest, InequalityOperator) {
	EXPECT_EQ(hasValue != eolToken, true);
}

//end of tests
