/*
 * InputList.h
 * Defines unit tests for the InputToken class
 * Created on 7/23/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/InputToken.h"
#include "../../src/cmd/InputList.h"
#include "../../src/cmd/InputIterator.h"
#include <gtest/gtest.h>

//test class
class InputListTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		InputListTest()
			: list()
		{
			//no code needed
		}

		//destructor
		~InputListTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			list.add(Orion::InputToken("STR", "Hello"));
			list.add(Orion::InputToken("STR", "World"));
			list.add(Orion::InputToken("INT", "42"));
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::InputList list; //the list to test
};

//this test checks the length method
TEST_F(InputListTest, Length) {
	EXPECT_EQ(list.length(), 3);
}

//this test checks the add methpd
TEST_F(InputListTest, Add) {
	EXPECT_EQ(list.length(), 3);
	list.add(Orion::InputToken("FLT", "3.14"));
	EXPECT_EQ(list.length(), 4);
}

//this test checks the get method
TEST_F(InputListTest, Get) {
	EXPECT_EQ(list.get(0), Orion::InputToken("STR", "Hello"));
}

//this test checks the isEmpty method
TEST_F(InputListTest, IsEmpty) {
	EXPECT_EQ(list.isEmpty(), false);
}

//this test checks the iterator
TEST_F(InputListTest, Iterator) {
	auto iter = list.begin();
	EXPECT_EQ(*iter, Orion::InputToken("STR", "Hello"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("STR", "World"));
	iter++;
	EXPECT_EQ(*iter, Orion::InputToken("INT", "42"));
	iter++;
	EXPECT_EQ(iter != list.end(), false);
}

//end of tests
