/*
 * GrammarGraph.h
 * Defines unit tests for the GrammarGraph class
 * Created on 7/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/cmd/GrammarGraph.h"
#include <gtest/gtest.h>

//test class
class GrammarGraphTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		GrammarGraphTest()
			: graph("START")
		{
			//no code needed
		}

		//destructor
		~GrammarGraphTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//assemble the graph
			graph.addNode("START", "STR");
			graph.addNode("STR", "INT");
			graph.addNode("STR", "ID");
			graph.addNode("INT", "START");
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::GrammarGraph graph;
};

//this test checks the follows method
TEST_F(GrammarGraphTest, Follows) {
	EXPECT_EQ(graph.follows("START", "STR"), true);
	EXPECT_EQ(graph.follows("STR", "INT"), true);
	EXPECT_EQ(graph.follows("INT", "START"), true);
	EXPECT_EQ(graph.follows("STR", "START"), false);
	EXPECT_EQ(graph.follows("STR", "ID"), true);
}

//this test checks the hasNode method
TEST_F(GrammarGraphTest, HasNode) {
	EXPECT_EQ(graph.hasNode("INT"), true);
	EXPECT_EQ(graph.hasNode("NUM"), false);
}

//this test checks the addNode method
TEST_F(GrammarGraphTest, AddNode) {
	//test adding a node to an existing parent
	EXPECT_EQ(graph.hasNode("NUM"), false);
	EXPECT_EQ(graph.addNode("ID", "NUM"), true);
	EXPECT_EQ(graph.hasNode("NUM"), true);

	//test adding a node to a nonexistent parent
	EXPECT_EQ(graph.hasNode("EXP"), false);
	EXPECT_EQ(graph.hasNode("VAL"), false);
	EXPECT_EQ(graph.addNode("EXP", "VAL"), false);
	EXPECT_EQ(graph.hasNode("VAL"), false);
}

//end of tests
