/*
 * DieLexer.h
 * Defines unit tests for the DieLexer class
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/rand/DieToken.h"
#include "../../src/rand/DieLexer.h"
#include <string>
#include <gtest/gtest.h>

//test class
class DieLexerTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DieLexerTest()
			: eqn("(1d6 + 4) - 1D20 + 120"), lexer(eqn)
		{
			//no code needed
		}

		//destructor
		~DieLexerTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		std::string eqn;
		Orion::DieLexer lexer;
};

//this test verifies the nextToken method
TEST_F(DieLexerTest, NextToken) {
	Orion::DieToken curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "LPAREN");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "1");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "DIE");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "6");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "PLUS");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "4");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "RPAREN");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "MINUS");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "1");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "DIE");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "20");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "PLUS");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), "INT");
	EXPECT_EQ(curToken.getValue(), "120");
	curToken = lexer.nextToken();
	EXPECT_EQ(curToken.getType(), Orion::DieToken::EOL.getType());
}

//end of tests
