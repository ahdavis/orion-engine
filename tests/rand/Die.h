/*
 * Die.h
 * Defines unit tests for the Die class
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/rand/Die.h"
#include <gtest/gtest.h>

//test class
class DieTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DieTest()
			: d20(), d6(6)
		{
			//no code needed
		}

		//destructor
		~DieTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::Die d20;
		Orion::Die d6;
};

//this test verifies that the default side count is 20
TEST_F(DieTest, DefaultIsD20) {
	EXPECT_EQ(d20.getSides(), 20);
}

//this test verifies that setting the number of sides
//to an invalid number sets the number of sides to 20
TEST_F(DieTest, InvalidSideCountSet) {
	EXPECT_EQ(d6.getSides(), 6);
	d6.setSides(-1);
	EXPECT_EQ(d6.getSides(), 20);
}

//end of tests
