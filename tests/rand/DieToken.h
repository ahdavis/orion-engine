/*
 * DieToken.h
 * Defines unit tests for the DieToken class
 * Created on 7/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/rand/DieToken.h"
#include <gtest/gtest.h>

//test class
class DieTokenTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DieTokenTest()
			: eolToken(Orion::DieToken::EOL)
		{
			//no code needed
		}

		//destructor
		~DieTokenTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::DieToken eolToken;
};

//this test verifies that the EOL token has a type of "EOL"
TEST_F(DieTokenTest, EOLTokenType) {
	EXPECT_EQ(eolToken.getType(), "EOL");
}

//this test verifies that the EOL token has an empty value
TEST_F(DieTokenTest, EOLTokenValue) {
	EXPECT_EQ(eolToken.getValue(), "");
}

//end of tests
