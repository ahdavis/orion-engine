/*
 * DieEquation.h
 * Defines unit tests for the DieEquation class
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/rand/DieEquation.h"
#include <gtest/gtest.h>

//test class
class DieEquationTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DieEquationTest()
			: constEqn("42"), addEqn("2 + 2"),
				subEqn("4 - 3"), oneDieEqn("2d6"),
				addDieEqn("1d6 + 4"), subDieEqn("2d6 - 2"),
				twoDieAddEqn("1d6 + 2d4"),
				twoDieSubEqn("4d6 - 1d3"),
				knownRollEqn("3d1"),
				parenEqn("11 - (4 + 5)")
		{
			//no code needed
		}

		//destructor
		~DieEquationTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::DieEquation constEqn;
		Orion::DieEquation addEqn;
		Orion::DieEquation subEqn;
		Orion::DieEquation oneDieEqn;
		Orion::DieEquation addDieEqn;
		Orion::DieEquation subDieEqn;
		Orion::DieEquation twoDieAddEqn;
		Orion::DieEquation twoDieSubEqn;
		Orion::DieEquation knownRollEqn;
		Orion::DieEquation parenEqn;
};

//this test checks evaluation of a constant value equation
TEST_F(DieEquationTest, ConstValue) {
	EXPECT_EQ(constEqn.evaluate(), 42);
}

//this test checks evaluation of a constant addition equation
TEST_F(DieEquationTest, ConstAdd) {
	EXPECT_EQ(addEqn.evaluate(), 4);
}

//this test checks evaluation of a constant subtraction equation
TEST_F(DieEquationTest, ConstSub) {
	EXPECT_EQ(subEqn.evaluate(), 1);
}

//this test checks evaluation of a single-die equation
TEST_F(DieEquationTest, OneDie) {
	int res = oneDieEqn.evaluate();
	EXPECT_GE(res, 2);
	EXPECT_LE(res, 12);
}

//this test checks evaluation of a one-die addition equation
TEST_F(DieEquationTest, OneDieAdd) {
	int res = addDieEqn.evaluate();
	EXPECT_GE(res, 5);
	EXPECT_LE(res, 10);
}

//this test checks evaluation of a one-die subtraction equation
TEST_F(DieEquationTest, OneDieSub) {
	int res = subDieEqn.evaluate();
	EXPECT_GE(res, 0);
	EXPECT_LE(res, 10);
}

//this test checks evaluation of a two-die addition equation
TEST_F(DieEquationTest, TwoDieAdd) {
	int res = twoDieAddEqn.evaluate();
	EXPECT_GE(res, 3);
	EXPECT_LE(res, 14);
}

//this test checks evaluation of a two-die subtraction equation
TEST_F(DieEquationTest, TwoDieSub) {
	int res = twoDieSubEqn.evaluate();
	EXPECT_GE(res, 1);
	EXPECT_LE(res, 23);
}

//this test checks evaluation of a known roll
TEST_F(DieEquationTest, KnownRoll) {
	int res = knownRollEqn.evaluate();
	EXPECT_EQ(res, 3);
}

//this test checks evaluation of an equation containing parentheses
TEST_F(DieEquationTest, Parentheses) {
	int res = parenEqn.evaluate();
	EXPECT_EQ(res, 2);
}

//end of tests
