/*
 * EventManager.h
 * Defines unit tests for the EventManager class
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/event/Event.h"
#include "../../src/event/EventType.h"
#include "../../src/event/EventManager.h"
#include <gtest/gtest.h>

//test class
class EventManagerTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		EventManagerTest()
		{
			//no code needed
		}

		//destructor
		~EventManagerTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//push an event onto the queue
			Orion::EventManager::getInstance()
				.pushEvent(Orion::Event(
					Orion::EventType::MapLoad));
		}

		//Shuts down the test suite
		void TearDown() override {
			Orion::EventManager::getInstance().clear();
		}

		//no fields
};

//this test checks the isEmpty method
TEST_F(EventManagerTest, IsEmpty) {
	EXPECT_EQ(Orion::EventManager::getInstance().isEmpty(), false);
}

//this test checks the clear method
TEST_F(EventManagerTest, Clear) {
	EXPECT_EQ(Orion::EventManager::getInstance().isEmpty(), false);
	Orion::EventManager::getInstance().clear();
	EXPECT_EQ(Orion::EventManager::getInstance().isEmpty(), true);
}

//this test checks the pollEvent method
TEST_F(EventManagerTest, PollEvent) {
	Orion::EventManager::getInstance().pushEvent(
			Orion::Event(Orion::EventType::RoomEnter));
	Orion::Event curEvent;
	EXPECT_EQ(Orion::EventManager::getInstance().pollEvent(curEvent),
			true);
	EXPECT_EQ(curEvent.getType(), Orion::EventType::MapLoad);
	EXPECT_EQ(Orion::EventManager::getInstance().pollEvent(curEvent),
			false);
	EXPECT_EQ(curEvent.getType(), Orion::EventType::RoomEnter);
	EXPECT_EQ(Orion::EventManager::getInstance().pollEvent(curEvent),
			false);
	EXPECT_EQ(curEvent.getType(), Orion::EventType::RoomEnter);
}

//this test checks the pushEvent method
TEST_F(EventManagerTest, PushEvent) {
	Orion::EventManager::getInstance().clear();
	EXPECT_EQ(Orion::EventManager::getInstance().isEmpty(), true);
	Orion::EventManager::getInstance().pushEvent(
			Orion::Event(Orion::EventType::EntityDie));
	EXPECT_EQ(Orion::EventManager::getInstance().isEmpty(), false);
}

//end of tests
