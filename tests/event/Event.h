/*
 * Event.h
 * Defines unit tests for the Event class
 * Created on 8/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/event/Event.h"
#include "../../src/event/EventType.h"
#include <gtest/gtest.h>

//test class
class EventTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		EventTest()
			: nullEvent()
		{
			//no code needed
		}

		//destructor
		~EventTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::Event nullEvent;
};

//this test verifies that the default constructor
//creates an Event with type NullEvent
TEST_F(EventTest, DefaultIsNull) {
	EXPECT_EQ(nullEvent.getType(), Orion::EventType::NullEvent);
}

//end of tests
