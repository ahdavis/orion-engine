/*
 * Map.h
 * Defines unit tests for the Map class
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/map/Direction.h"
#include "../../src/map/Map.h"
#include "../../src/map/Room.h"
#include <gtest/gtest.h>

//test class
class MapTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		MapTest()
			: map(nullptr)
		{
			//no code needed
		}

		//destructor
		~MapTest() override {
			delete map;
			map = nullptr;
		}

		//Sets up the test suite
		void SetUp() override {
			//create the start room
			Orion::Room start("Start Room", 
					"Looking down a dark hallway.");

			//and initialize the map
			map = new Orion::Map(start);
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::Map* map;
};

//this test checks the addRoom methods
TEST_F(MapTest, AddRoom) {
	EXPECT_EQ(map->getRoomCount(), 1);
	Orion::Room room2("Dark Hallway", "A dimly lit hallway");
	EXPECT_EQ(map->addRoom("Start Room", room2,
				Orion::Direction::North), true);
	EXPECT_EQ(map->getRoomCount(), 2);
	EXPECT_EQ(map->addRoom(0, room2,
				Orion::Direction::North), false);
	EXPECT_EQ(map->getRoomCount(), 2);
}

//this test checks the move method
TEST_F(MapTest, Move) {
	Orion::Room room2("Dark Hallway", "A dimly lit hallway");
	EXPECT_EQ(map->addRoom("Start Room", room2, 
				Orion::Direction::North), true);
	EXPECT_EQ(map->move(Orion::Direction::South), false);
	EXPECT_EQ(map->move(Orion::Direction::North), true);
	EXPECT_EQ(map->move(Orion::Direction::South), true);
}

//this test checks the hasRoom methods
TEST_F(MapTest, HasRoom) {
	EXPECT_EQ(map->hasRoom(0), true);
	EXPECT_EQ(map->hasRoom("Start Room"), true);
	EXPECT_EQ(map->hasRoom(1), false);
	EXPECT_EQ(map->hasRoom("Dark Hallway"), false);
	Orion::Room room2("Dark Hallway", "A dimly lit hallway");
	map->addRoom(0, room2, Orion::Direction::North);
	EXPECT_EQ(map->hasRoom(1), true);
	EXPECT_EQ(map->hasRoom("Dark Hallway"), true);
}

//end of tests
