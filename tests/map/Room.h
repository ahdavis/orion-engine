/*
 * Room.h
 * Defines unit tests for the Room class
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/map/Direction.h"
#include "../../src/map/Room.h"
#include <gtest/gtest.h>

//test class
class RoomTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		RoomTest()
			: room("Starting Room",
				"You are in a bare, rocky chamber.")
		{
			//no code needed
		}

		//destructor
		~RoomTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::Room room;
};

//this test checks the hasDoorInDirection method
TEST_F(RoomTest, HasDoorInDirection) {
	//check for a door in the north
	EXPECT_EQ(room.hasDoorInDirection(Orion::Direction::North), false);
}

//this test checks the addDoor method
TEST_F(RoomTest, AddDoor) {
	//check for a door in the north
	EXPECT_EQ(room.hasDoorInDirection(Orion::Direction::North), false);

	//add a door in the north
	Orion::Room nRoom("North Room", "You see light to the north.");
	EXPECT_EQ(nRoom.hasDoorInDirection(Orion::Direction::South),
			false);
	room.addDoor(nRoom, Orion::Direction::North);

	//and check for a door in the north of the start room
	//and in the south of the north room
	EXPECT_EQ(room.hasDoorInDirection(Orion::Direction::North), true);
	EXPECT_EQ(nRoom.hasDoorInDirection(Orion::Direction::South), true);
}

//this test checks that multiple doors cannot
//be added in the same direction
TEST_F(RoomTest, SameDirectionAdd) {
	//add a door in the north
	Orion::Room nRoom("North Room", "You see light to the north.");
	EXPECT_EQ(room.addDoor(nRoom, Orion::Direction::North), true);

	//attempt to add another door in the north
	Orion::Room nRoom2("North Room 2", "You see light to the north.");
	EXPECT_EQ(room.addDoor(nRoom2, Orion::Direction::North), false);
}

//this test checks that multiples of the same room cannot be connected
//to one room
TEST_F(RoomTest, NonUniqueAdd) {
	//create a room
	Orion::Room nRoom("Other Room", "This room is filled with water.");

	//and attempt to add it twice
	EXPECT_EQ(room.addDoor(nRoom, Orion::Direction::North), true);
	EXPECT_EQ(room.addDoor(nRoom, Orion::Direction::South), false);
}

//end of tests
