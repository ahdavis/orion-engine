/*
 * Direction.h
 * Defines unit tests for the Direction enum
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/map/Direction.h"
#include <gtest/gtest.h>

//test class
class DirectionTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DirectionTest()
			: north(Orion::Direction::North),
				east(Orion::Direction::East),
				south(Orion::Direction::South),
				west(Orion::Direction::West),
				neast(Orion::Direction::Northeast),
				nwest(Orion::Direction::Northwest),
				seast(Orion::Direction::Southeast),
				swest(Orion::Direction::Southwest)
		{
			//no code needed
		}

		//destructor
		~DirectionTest() override {
			//no code needed
		}

		//Sets up the test suite
		void SetUp() override {
			//no code needed
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//fields
		Orion::Direction north;
		Orion::Direction east;
		Orion::Direction south;
		Orion::Direction west;
		Orion::Direction neast;
		Orion::Direction nwest;
		Orion::Direction seast;
		Orion::Direction swest;
};

//this test checks getting the names of the directions
TEST_F(DirectionTest, Name) {
	EXPECT_EQ(Orion::nameForDirection(north), "North");
	EXPECT_EQ(Orion::nameForDirection(east), "East");
	EXPECT_EQ(Orion::nameForDirection(south), "South");
	EXPECT_EQ(Orion::nameForDirection(west), "West");
	EXPECT_EQ(Orion::nameForDirection(neast), "Northeast");
	EXPECT_EQ(Orion::nameForDirection(nwest), "Northwest");
	EXPECT_EQ(Orion::nameForDirection(seast), "Southeast");
	EXPECT_EQ(Orion::nameForDirection(swest), "Southwest");
}


//end of tests
