/*
 * Door.h
 * Defines unit tests for the Door class
 * Created on 7/29/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../../src/map/Direction.h"
#include "../../src/map/Door.h"
#include "../../src/map/Room.h"
#include <gtest/gtest.h>

//test class
class DoorTest : public ::testing::Test {
	//protected fields and methods
	protected:
		//constructor
		DoorTest()
			: door(nullptr)
		{
			//no code needed
		}

		//destructor
		~DoorTest() override {
			delete door;
			door = nullptr;
		}

		//Sets up the test suite
		void SetUp() override {
			Orion::Room r1("Room One", "Room One");
			Orion::Room r2("Room Two", "Room Two");
			door = new Orion::Door(r1, r2, 
					Orion::Direction::North);
		}

		//Shuts down the test suite
		void TearDown() override {
			//no code needed
		}

		//field
		Orion::Door* door;
};

//this test checks the getEntryDirection method
TEST_F(DoorTest, EntryDirection) {
	EXPECT_EQ(door->getEntryDirection(), Orion::Direction::South);
}

//this test checks the getExitDirection method
TEST_F(DoorTest, ExitDirection) {
	EXPECT_EQ(door->getExitDirection(), Orion::Direction::North);
}


//end of tests
